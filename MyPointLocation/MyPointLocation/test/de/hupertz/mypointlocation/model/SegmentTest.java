/*
*	Copyright (C) by Julian Hupertz <julian.hupertz@gmail.com>
*	
*	This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*   
*/

package de.hupertz.mypointlocation.model;

import org.junit.Test;
import static org.junit.Assert.*;

public class SegmentTest {

	@Test
	public void testCreateSegment() {
		Point p1 = new Point(10,15);
		Point p2 = new Point(11,16);
		
		Segment s1 = new Segment(p1, p2);
		assertTrue(p1.equals(s1.getStart()));
		assertTrue(p2.equals(s1.getEnd()));
		
		Segment s2 = new Segment(p2, p1);
		assertTrue(p1.equals(s2.getStart()));
		assertTrue(p2.equals(s2.getEnd()));
		assertTrue(s1.getGradient() == (p1.getY() - p2.getY()) / (p1.getX() - p2.getX()));
		assertTrue(s2.getGradient() == (p2.getY() - p1.getY()) / (p2.getX() - p1.getX()));
		assertTrue(s2.getAxis() == s2.getStart().getY() 
				- (p2.getY() - p1.getY()) / (p2.getX() - p1.getX()) 
				* s2.getStart().getX());
		assertTrue(s2.getMiddle().equals(new Point((p1.getX() + p2.getX()) / 2,
				(p1.getY() + p2.getY()) / 2)));
		
		Point p3 = new Point(8,7);
		Point p4 = new Point(9,8);
		Segment s3 = new Segment(p3, p4);
		assertTrue(s1.compareTo(s1) == 0);
		assertTrue(s1.compareTo(s3) == 1);
		assertTrue(s3.compareTo(s1) == -1);
		
		
		
	}
}
