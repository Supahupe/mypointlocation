/*
*	Copyright (C) by Julian Hupertz <julian.hupertz@gmail.com>
*	
*	This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*   
*/

package de.hupertz.mypointlocation.model;

import org.junit.Test;
import static org.junit.Assert.*;

public class PointTest {

	@Test
	public void testPointCreation() {
		Point p1 = new Point(10, 33);
		assertTrue(p1.getX() == 10);
		assertTrue(p1.getY() == 33);
		
		Point p2 = new Point(10.0003, 33.0003);
		assertTrue(p2.getX() == 10.0);
		assertTrue(p2.getY() == 33.0);
		assertTrue(p1.equals(p2));
		assertTrue(p2.equals(p1));
		
		Point p3 = new Point(p2);
		assertFalse(p2 == p3);
		assertTrue(p2.equals(p3));
		assertTrue(p3.equals(p2));
		
		Point p4 = new Point(11,34);
		assertFalse(p3.equals(p4));
		assertFalse(p4.equals(p3));	
		assertTrue(p3.compareTo(p3) == 0);
		assertTrue(p3.compareTo(p4) == -1);
		assertTrue(p4.compareTo(p3) == 1);
	}
	
	@Test(expected = NullPointerException.class)
	public void testCompareTo() {
		Point p = new Point(10, 10);
		p.compareTo(null);
	}
}
