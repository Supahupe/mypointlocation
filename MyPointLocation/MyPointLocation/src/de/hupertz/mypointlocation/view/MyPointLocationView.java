package de.hupertz.mypointlocation.view;
	
import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;


public class MyPointLocationView extends Application {
		
	@Override
	public void start(Stage stage) {
		Parent root = null;
		FXMLLoader loader = new FXMLLoader();
		
		loader.setLocation(getClass().
					getResource("point_location_gui.fxml"));
		
		try {
			root = (BorderPane) loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Scene scene = new Scene(root);
		
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		
		stage.setTitle("Masterprojekt ZIS Punktlokalisierung");
		stage.getIcons().add(new Image(getClass().getResourceAsStream("icon.JPG")));
		stage.setScene(scene);
		stage.show();
	}
	
	@Override
	public void init() throws Exception {
		// TODO Auto-generated method stub
		super.init();
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
