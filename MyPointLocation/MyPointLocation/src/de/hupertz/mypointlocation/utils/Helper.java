package de.hupertz.mypointlocation.utils;

import de.hupertz.mypointlocation.model.Point;
import de.hupertz.mypointlocation.model.Segment;

public class Helper {
	
	//TODO: Eventuell wietere Methode einbauen, die Node zurückliefert
	public static Point isIntersection(Segment s1, Segment s2) {
		return isIntersection(s1.getStart(), s1.getEnd(), s2.getStart(), s2.getEnd());
	}
	
	/**
	 * Methode liefert 0 zurück, wenn die Steigung zweier Geraden gleich ist
	 * @param start1
	 * @param end1
	 * @param start2
	 * @param end2
	 * @return
	 */
	public static Point isIntersection(Point start1, Point end1, Point start2, Point end2) {
			
		if(start1.getX() == end1.getX()) {
			double gradient = (start2.getY() - end2.getY()) /  (start2.getX() - end2.getX());
			double axis = start2.getY() - gradient * start2.getX();
			double y = gradient * start1.getX() + axis;
			
			return new Point(start1.getX(), y);
		
		} else if (start2.getX() == end2.getX()) {	
			double gradient = (start1.getY() - end1.getY()) /  (start1.getX() - end1.getX());
			double axis = start1.getY() - gradient * start1.getX();
			double y = gradient * start2.getX() + axis;
			
			return new Point(start2.getX(), y);
			
		} 
		
		double gradient1 = (start1.getY() - end1.getY()) /  (start1.getX() - end1.getX());
		double axis1 = start1.getY() - gradient1 * start1.getX();
		double gradient2 = (start2.getY() - end2.getY()) /  (start2.getX() - end2.getX());
		double axis2 = start2.getY() - gradient2 * start2.getX();
			
		if(gradient1 == gradient2)
			return null;
			
		double intersecX = (axis2 - axis1) / (gradient1 -gradient2);
		double intersecY = gradient1 * intersecX + axis1;
			
		return new Point(intersecX, intersecY);
	}
}