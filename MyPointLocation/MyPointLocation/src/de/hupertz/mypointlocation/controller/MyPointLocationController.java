package de.hupertz.mypointlocation.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;
import java.util.ResourceBundle;

import com.sun.javafx.geom.Edge;

import de.hupertz.mypointlocation.model.Face;
import de.hupertz.mypointlocation.model.Point;
import de.hupertz.mypointlocation.model.SearchNode;
import de.hupertz.mypointlocation.model.Slab;
import de.hupertz.mypointlocation.model.simpleapproach.SimpleApproachDataStructure;
import de.hupertz.mypointlocation.model.trapecoidalmap.DirectedAcyclicGraph;
import de.hupertz.mypointlocation.model.trapecoidalmap.Trapecoid;
import de.hupertz.mypointlocation.model.trapecoidalmap.TrapecoidalMap;
import de.hupertz.mypointlocation.utils.Helper;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;


/**
 * Controller-Klasse
 * @author Julian
 *
 */
public class MyPointLocationController implements Initializable, Observer {
	
	private final String 				TITLE_CAREFUL = "Vorsicht";
	private final String 				MESSAGE_INITIALIZE = "Bitte initialisieren Sie die Zeichenfl�che.";
	private final String 				MESSAGE_INITIALIZE_DATASTRUCTURE = "Bitte erzeugen Sie zuerst die Datenstruktur, um Anfragen darauf zu beantworten.";
	private final String 				MESSAGE_SEGMENT = "Bitte zeichnen Sie mindestens eine Strecke auf der Zeichenfl�che.";
	private final String 				MESSAGE_SLABS = "Bitte erzeugen Sie \"Scheiben\" zur Bildung der naiven Datenstruktur.";
	private final String				MESSAGE_TRAPECOIDS_CREATED = "Es wurden bereits Trapeze erzeugt. Bitte erneuern Sie erst die Zeichenfl�che.";
	private final String				MESSAGE_SLABS_CREATED = "Es wurden bereits Scheiben erzeugt. Bitte erneuern Sie erst die Zeichenfl�che.";
	private final String 				BUTTON_PAINTINGAREA_INITIALIZE = "Initialisiere Zeichenfl�che";
	private final String 				BUTTON_PAINTINGAREA_DEINITIALIZE = "Deinitialisiere Zeichenfl�che";
	private final String 				CSS_BUTTON_GREEN = "buttonGreen";
	private final String 				CSS_BUTTON_RED = "buttonRed";
	
	private final int	 				LINE_WIDTH = 1;
	private final int	 				LINE_SLAB_WIDTH = 1;
	private final int	 				LINE_BOX_WIDTH = 5;
	private final int	 				RADIUS_CIRCLE = 5;
	private final int					EUCLIDIC_DISTANCE_GET_NEAREST_POINT = 20;
	
	private final Color  				COLOR_LIGHTGRAY = Color.LIGHTGRAY;
	private final Color  				COLOR_RED = Color.RED;
	private final Color  				COLOR_BLUE = Color.BLUE;
	private final Color  				COLOR_GREEN = Color.GREEN;
	
	private boolean 	 				mBorder = false;
	private boolean 	 				mSlabsCreated = false;
	private boolean						mExtensionsCreated = false;
	private boolean 	 				mNaiveDatastructureCreated = false;
	private boolean						mTrapecoidalDatastructureCreated = false;
	
	private int 		 				segmentPointCounter = 0;
	private int							mDebugViewIndex = 1;
	
	private double 		 				mWidth = 0;
	private double 		 				mHeigth = 0;
	
	private GraphicsContext 			mGc = null;
	private GraphicsContext				mGcDebug = null;
	
	private Point 						mPoints[] = new Point[2];
	
	private ArrayList<Point> 			mPointsList = new ArrayList<Point>();
	private ArrayList<Edge> 			mSegmentsList = new ArrayList<Edge>();
	private ArrayList<Edge> 			mSlabSegmentsList = new ArrayList<Edge>();
	private ArrayList<Edge>			mExtensionSegmentsList = new ArrayList<Edge>();
	private ArrayList<Slab> 			mSlabList = new ArrayList<Slab>();
	
	private SimpleApproachDataStructure simpleApproachDatastructure;
	
	private TrapecoidalMap				trapecoidalMapDatastructure;
	
	@FXML protected Canvas 				mCanvasPaint;
	@FXML protected Canvas				mCanvasAnalyze;
	@FXML private MenuItem 				closeMenuItem;
	@FXML private ChoiceBox<String> 	mChoiceBox;
	@FXML private TreeView<String>		mTreeViewGraph;
	@FXML private Label					mLabelCounter;
	@FXML private Button				btnNextState;
	@FXML private Button				btnPrevState;
	
	private TreeItem<String> 			mRoot;
	
	private HashMap<Integer, HashMap<ArrayList<Trapecoid>, DirectedAcyclicGraph>> trapecoidalMapStates;
	
	/**
	 * Initalisieren der GUI
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		//Erzeuge leere Zeichenfl�che und die notwendigen Datenstrukturen
		mPoints[0] = null;
		mPoints[1] = null;
		mSegmentsList.clear();
		mPointsList.clear();
		mSlabSegmentsList.clear();
		mSlabList.clear();
		mExtensionSegmentsList.clear();
		mWidth = mCanvasPaint.getWidth();
		mHeigth = mCanvasPaint.getHeight();
		mGc = mCanvasPaint.getGraphicsContext2D();
		mGcDebug = mCanvasAnalyze.getGraphicsContext2D();
		drawRectangle(mGc, COLOR_LIGHTGRAY, COLOR_RED, LINE_BOX_WIDTH, mWidth, mHeigth);
		simpleApproachDatastructure = new SimpleApproachDataStructure(mWidth, mHeigth);
		trapecoidalMapDatastructure = new TrapecoidalMap(mWidth, mHeigth);
		
		trapecoidalMapStates = new HashMap<Integer,HashMap<ArrayList<Trapecoid>, DirectedAcyclicGraph>>();
		
		mLabelCounter.setText(""+1);
		
		mRoot = new TreeItem<String>("Zust�nde");
		mRoot.setExpanded(false);
		
		mTreeViewGraph.setRoot(mRoot);
		
		trapecoidalMapDatastructure.addObserver(this);
	}
	
	/**
	 * Initalisieren der Zeichenfl�che
	 * @param e
	 */
	@FXML protected void initializePaintingArea(ActionEvent e) {
		
		Button b = (Button)e.getSource();
		
		if(!mBorder) {

			drawRectangle(mGc, COLOR_LIGHTGRAY, COLOR_GREEN, LINE_BOX_WIDTH, mWidth, mHeigth);
			mBorder = true;
			b.setText(BUTTON_PAINTINGAREA_DEINITIALIZE);
			b.setId(CSS_BUTTON_GREEN);
			
		} else {
			
			clearPaintingAreaAndDatastructure();
			b.setText(BUTTON_PAINTINGAREA_INITIALIZE);
			b.setId(CSS_BUTTON_RED);
			
		}
	}
	
	/**
	 * Mausklick auf der Canvas-Zeichenfl�che
	 * @param e
	 */
	@FXML protected void mouseOnCanvasPressed(MouseEvent e) {
		
		//Koordinaten des Mausklick abfangen
		double clickedX = e.getX();
		double clickedY = e.getY();
		
		/*
		 * Diverse Checks, je nachdem, in welchem Zustand die GUI ist und was gedr�ckt wurde
		 */
		
		
		if(e.getButton().equals(MouseButton.SECONDARY) && mBorder) { //Rechte Maustaste, Zeichenfl�che initialisiert
			
			clearPaintingAreaAndDatastructure();
			return;
			
		} else if(e.getButton().equals(MouseButton.SECONDARY) && !mBorder) { //Rechte Maustaste, Zeichenfl�che ist nicht initalisiert
			
			createAlertMessage(AlertType.INFORMATION, TITLE_CAREFUL, MESSAGE_INITIALIZE);
			return;
			
		} else if(e.getButton().equals(MouseButton.MIDDLE) && !mBorder) { //Mittlere Maustaste, Zeichenfl�che ist nicht initialisiert
			
			createAlertMessage(AlertType.INFORMATION, TITLE_CAREFUL, MESSAGE_INITIALIZE);
			return;
			
		} else if(e.getButton().equals(MouseButton.MIDDLE) && mBorder && (!mNaiveDatastructureCreated && !mTrapecoidalDatastructureCreated)) { //mittlere Maustaste,
			//Zeichenfl�che ist initialisiert, Datenstrukturen sind aber noch nicht erzeugt worden
			
			createAlertMessage(AlertType.INFORMATION, TITLE_CAREFUL, MESSAGE_INITIALIZE_DATASTRUCTURE);
			return;
			
		} else if(e.getButton().equals(MouseButton.MIDDLE) && mNaiveDatastructureCreated) { //mittlere Mausetaste und Datenstruktur f�r naiven Ansatz wurde erzeugt

			Face f = this.simpleApproachDatastructure.binarySearch(mSlabList, 0, mSlabList.size(), new Point(clickedX, clickedY));
			
			if (f!=null) {
				createAlertMessage(AlertType.INFORMATION, "Super", ""+f.getID());
			} else {
				createAlertMessage(AlertType.INFORMATION, "Ups", "Bei der Suche ist etwas schief gelaufen.");
			}
			
			return;
			
		} else if(e.getButton().equals(MouseButton.MIDDLE) && mTrapecoidalDatastructureCreated) { //mittlere Maustaste gedr�ckt und Datenstruktur f�r Trapezkartenansatz erzeugt
			
			SearchNode t=null;
			try {
				t = this.trapecoidalMapDatastructure.findTrapecoid(false, new Point(clickedX, clickedY), null);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
			if (t!=null && t instanceof Trapecoid) {
				createAlertMessage(AlertType.INFORMATION, "Super", ""+((Trapecoid)t).getID());
			} else {
				createAlertMessage(AlertType.INFORMATION, "Ups", "Bei der Suche ist etwas schief gelaufen oder Anfragepunkt liegt auf Segment.");
			}
			
			return;
		}
		
		if(mBorder) { //mBorder ist true, dann kann gezeichnet werden
			
			Point p = getNearestPoint(clickedX, clickedY); //wenn der Klick in der N�he eines vorhandenen Punktes stattfindet, wird der vorhandene Punkt zur�ckgeliefert

			double x = clickedX;
			double y = clickedY;
				
			if(!(p==null) && segmentPointCounter < 1) { //Ein Punkt in der N�he wurde gefunden; erster Endpunkt eines Segmentes
				
				x = p.getX();
				y = p.getY();
				
			} else if(!(p==null) && segmentPointCounter == 1) { //Ein Punkt in der N�he wurde gefunden; zweiter Endpunkt eines Segmentes
				
				x = p.getX();
				y = p.getY();
				
				if(checkDifference(mPoints[0], new Point(x, y))) { // Pr�fe Distanz zwischen bereits vorhandenem Punkt und geklicktem Punkt
					
					return;
					
				}
				
			} 
			//Vorsicht, ge�ndert
			if (!(mPoints[0]==null)) { // pr�ft, dass neu geklickter Punkt nicht zu nah an dem bereits vorhandenem Punkt ist 
				
				if(checkDifference(mPoints[0], new Point(x, y))) { // Pr�fe Distanz zwischen bereits vorhandenem Punkt und geklicktem Punkt
					
					return;
					
				}
			} 
			
			drawPoint(mGc, x, y, null, COLOR_RED, LINE_WIDTH, RADIUS_CIRCLE); //zeichne Punkt
			segmentPointCounter++;
			
			if(segmentPointCounter == 1) { //wenn der erste Punkt f�r eine Gerade gezeichnet wird
				
				mPoints[0] = (p==null) ? new Point(x, y) : p;
				
				if(!(mPointsList.contains(mPoints[0]))) {
					mPointsList.add(mPoints[0]);
				}
				
			} else  { //wenn der zweite Punkt f�r eine Gerade gezeichnet ist
				
				mPoints[1] = (p==null) ? new Point(x, y) : p;
				
				if(!(mPointsList.contains(mPoints[1]))) {
					mPointsList.add(mPoints[1]);
				}
				
				Edge newSeg = new Edge(mPoints[0], mPoints[1]);
				mSegmentsList.add(newSeg);
				
				drawLine(mGc, mPoints[0], mPoints[1], COLOR_BLUE, LINE_WIDTH);
				
				mPoints[0] = null;
				mPoints[1] = null;
				segmentPointCounter = 0;
				
				ArrayList<Edge> tempAddList = new ArrayList<Edge>();
				ArrayList<Edge> tempRemoveList = new ArrayList<Edge>();
				
				HashMap<Point, Edge[]> intersections = getIntersections(newSeg);
				
				updateIntersectedSegments(intersections, tempAddList, tempRemoveList, newSeg);
			}
			
		} else {
			
			createAlertMessage(AlertType.INFORMATION, TITLE_CAREFUL, MESSAGE_INITIALIZE);
			return;
			
		}

	}
	
	/**
	 * Erzeuge zuf�llige Segmente auf der Karte
	 * @param e
	 */
	@FXML protected void createRandomSegments(ActionEvent e) {
		
		if(!mBorder) {
			
			createAlertMessage(AlertType.INFORMATION, TITLE_CAREFUL, MESSAGE_INITIALIZE);
			return;
			
		}
		
		double count = Double.parseDouble(mChoiceBox.getSelectionModel().getSelectedItem());
		double x = mWidth;
		double y = mHeigth;
		double xt = 0;
		double yt = 0;
		
		for(int j=0;j<count*2;j++) {
			
			if(j % 2 == 0) {
				
				Random r = new Random();
				xt = r.nextDouble()*x;
				yt = r.nextDouble()*y;
				Point p = getNearestPoint(xt, yt);
				
				if(p==null) {
					
					mPoints[0] = drawPoint(mGc, xt, yt, COLOR_LIGHTGRAY, COLOR_RED, LINE_WIDTH, RADIUS_CIRCLE);
					
				} else {
					
					mPoints[0] = p;
					
				}
				
				if(!(mPointsList.contains(mPoints[0]))) {
					mPointsList.add(mPoints[0]);
				}
				
			} else {
				
				Random r = new Random();
				xt = r.nextDouble()*x;
				yt = r.nextDouble()*y;
				Point p = getNearestPoint(xt, yt);
				
				if(p==null) {
					
					mPoints[1] = drawPoint(mGc, xt, yt, COLOR_LIGHTGRAY, COLOR_RED, LINE_WIDTH, RADIUS_CIRCLE);
					
				} else {
					
					mPoints[1] = p;
					
				}
				
				if(!(mPointsList.contains(mPoints[1]))) {
					mPointsList.add(mPoints[1]);
				}
				
				Edge newSeg = new Edge(mPoints[0], mPoints[1]);
				mSegmentsList.add(newSeg);
				drawLine(mGc, mPoints[0], mPoints[1], COLOR_BLUE, LINE_WIDTH);
				mPoints[0] = null;
				mPoints[1] = null;
				
				ArrayList<Edge> tempAddList = new ArrayList<Edge>();
				ArrayList<Edge> tempRemoveList = new ArrayList<Edge>();
				
				HashMap<Point, Edge[]> intersections = getIntersections(newSeg);
				
				updateIntersectedSegments(intersections, tempAddList, tempRemoveList, newSeg);
				
			}
		}
	}
	
	/**
	 * Erzeuge Scheiben f�r den naiven Algorithmus
	 * @param e
	 */
	@FXML protected void createSlabsForSimpleApproach(ActionEvent e) {
		
		if(!mBorder) {
			
			createAlertMessage(AlertType.INFORMATION, TITLE_CAREFUL, MESSAGE_INITIALIZE);
			return;
			
		} else if (mSegmentsList.size()==0 && !mExtensionsCreated) {
			
			createAlertMessage(AlertType.INFORMATION, TITLE_CAREFUL, MESSAGE_SEGMENT);
			return;
			
		} else if (mExtensionsCreated && mSegmentsList.size() == 0) {
			createAlertMessage(AlertType.INFORMATION, TITLE_CAREFUL, MESSAGE_TRAPECOIDS_CREATED);
			return;
		}
		
		for(Point p : mPointsList) {
			drawLine(mGc, p, new Point(p.getX(), 0), COLOR_RED, LINE_SLAB_WIDTH);
			drawLine(mGc, p, new Point(p.getX(), mHeigth), COLOR_RED, LINE_SLAB_WIDTH);
		}
		
		Collections.sort(mPointsList);
		
		for(int i=0; i<mPointsList.size(); i++) { 
			
			if (i==0) {
				
				Point p1 = new Point(0, 0);
				Point p2 = new Point(0, mHeigth);
				Edge s1 = new Edge(p1, p2);
				Point p3 = new Point(mPointsList.get(i).getX(), 0);
				Point p4 = new Point(mPointsList.get(i).getX(), mHeigth);
				Edge s2 = new Edge(p3, p4);
				Slab s = new Slab(s1, s2);
				mSlabList.add(s);
				
				p1 = new Point(mPointsList.get(i).getX(), 0);
				p2 = new Point(mPointsList.get(i).getX(), mHeigth);
				s1 = new Edge(p1, p2);
				p3 = new Point(mPointsList.get(i+1).getX(), 0);
				p4 = new Point(mPointsList.get(i+1).getX(), mHeigth);
				s2 = new Edge(p3, p4);
				s = new Slab(s1, s2);
				mSlabList.add(s);
				
			} else if (i==mPointsList.size()-1) {
				
				Point p1 = new Point(mWidth, 0);
				Point p2 = new Point(mWidth, mHeigth);
				Edge s1 = new Edge(p1, p2);
				Point p3 = new Point(mPointsList.get(i).getX(), 0);
				Point p4 = new Point(mPointsList.get(i).getX(), mHeigth);
				Edge s2 = new Edge(p3, p4);
				Slab s = new Slab(s2, s1);
				mSlabList.add(s);
				
			} else {
				
				Point p1 = new Point(mPointsList.get(i).getX(), 0);
				Point p2 = new Point(mPointsList.get(i).getX(), mHeigth);
				Edge s1 = new Edge(p1, p2);
				Point p3 = new Point(mPointsList.get(i+1).getX(), 0);
				Point p4 = new Point(mPointsList.get(i+1).getX(), mHeigth);
				Edge s2 = new Edge(p3, p4);
				Slab s = new Slab(s1, s2);
				mSlabList.add(s);
				
			}
		}
		
		mSlabsCreated = true;
		
	}
	
	/**
	 * Erzeuge Datenstruktur f�r naiven Algorithmus
	 * @param e
	 */
	@FXML protected void createNaiveDatastructure(ActionEvent e) {
		
		if(!mBorder) {
			
			createAlertMessage(AlertType.INFORMATION, TITLE_CAREFUL, MESSAGE_INITIALIZE);
			return;
			
		} else if (!mSlabsCreated) {
			
			createAlertMessage(AlertType.INFORMATION, TITLE_CAREFUL, MESSAGE_SLABS);
			return;
			
		} else {
			
			mSlabList = this.simpleApproachDatastructure.createDataStructure(mPointsList, mSegmentsList, mSlabList);
			mNaiveDatastructureCreated = true;
		}
	}
	
	/**
	 * Helper-Method; speichert key-value-Paare der Schnittpunkts und der je schneidenen Strecken
	 * @param s
	 * @return
	 */
	private HashMap<Point, Edge[]> getIntersections(Edge s) {
		
		HashMap<Point, Edge[]> intersections = new HashMap<Point, Edge[]>();
		
		for(int i = 0; i<mSegmentsList.size(); i++) {
			
			if(mSegmentsList.size() == 1) {
				continue;
			}
			
			Edge s1 = s;
			Edge s2 = mSegmentsList.get(i);
			
			Point interP = Helper.isIntersection(s1, s2);
			
			if((interP != null) && 
					((interP.getX() > s1.getStart().getX() && interP.getX() < s1.getEnd().getX()) ||
							interP.getX() < s1.getStart().getX() && interP.getX() > s1.getEnd().getX()) &&
					((interP.getX() > s2.getStart().getX() && interP.getX() < s2.getEnd().getX()) ||
							interP.getX() < s2.getStart().getX() && interP.getX() > s2.getEnd().getX())
			) {
				
				Edge arr[] = new Edge[2];
				
				arr[0] = s1;
				arr[1] = s2;
				
				intersections.put(interP, arr);
				drawPoint(mGc, interP.getX(), interP.getY(), null, COLOR_RED, LINE_WIDTH, RADIUS_CIRCLE);
				
			}
		}
		
		return intersections;
	}
	
	/**
	 * Helper-Methode; aktualisiert die vorhanden Segmente und Datenstruturen f�r den naiven Ansatz, wenn sich zwei Segmente geschnitten haben
	 * @param intersections
	 * @param tempAddList
	 * @param tempRemoveList
	 * @param newSeg
	 */
	private void updateIntersectedSegments(HashMap<Point, Edge[]> intersections, ArrayList<Edge> tempAddList, ArrayList<Edge> tempRemoveList, Edge newSeg) {
		
		if(intersections.size()>1) {
			ArrayList<Point> tempList = new ArrayList<Point>();
			tempList.addAll(intersections.keySet());
			Point prev=null;
			Point act=null;
			
			Collections.sort(tempList);
			
			boolean flag = false;
			
			for(int i=0; i< tempList.size(); i++) {
				
				Edge newSegment = intersections.get(tempList.get(i))[0];
				Edge prevSegment = intersections.get(tempList.get(i))[1];
				
				if(i==0) {
					
					act = tempList.get(i);
					
					if((newSegment.getStart().getX()-act.getX() < (newSegment.getEnd().getX() - act.getX()))) {
						
						tempAddList.add(new Edge(newSegment.getStart(), act));
						tempAddList.add(new Edge(prevSegment.getStart(), act));
						tempAddList.add(new Edge(prevSegment.getEnd(), act));
						tempRemoveList.add(newSegment);
						tempRemoveList.add(prevSegment);
						flag = true;
						
					} else {
						
						tempAddList.add(new Edge(newSegment.getEnd(), act));
						tempAddList.add(new Edge(prevSegment.getStart(), act));
						tempAddList.add(new Edge(prevSegment.getEnd(), act));
						tempRemoveList.add(newSegment);
						tempRemoveList.add(prevSegment);
						flag = false;
						
					}
					
				} else if(i==tempList.size() -1 ) {
					prev = tempList.get(i-1);
					act = tempList.get(i);
					
					if(flag) {
						
						tempAddList.add(new Edge(prev, act));
						tempAddList.add(new Edge(newSegment.getEnd(), act));
						tempAddList.add(new Edge(prevSegment.getStart(), act));
						tempAddList.add(new Edge(prevSegment.getEnd(), act));
						tempRemoveList.add(newSegment);
						tempRemoveList.add(prevSegment);
						
					} else {
						
						tempAddList.add(new Edge(prev, act));
						tempAddList.add(new Edge(newSegment.getStart(), act));
						tempAddList.add(new Edge(prevSegment.getStart(), act));
						tempAddList.add(new Edge(prevSegment.getEnd(), act));
						tempRemoveList.add(newSegment);
						tempRemoveList.add(prevSegment);
						
					}
					
				} else {
					prev = tempList.get(i-1);
					act = tempList.get(i);
					
					tempAddList.add(new Edge(prev, act));
					tempAddList.add(new Edge(prevSegment.getStart(), act));
					tempAddList.add(new Edge(prevSegment.getEnd(), act));
					tempRemoveList.add(prevSegment);
					
				}
			}
			
		} else if (intersections.size() == 1) {
			
			for(Point act : intersections.keySet()) {
				tempAddList.add(new Edge(newSeg.getStart(), act));
				tempAddList.add(new Edge(newSeg.getEnd(), act));
				tempAddList.add(new Edge(intersections.get(act)[1].getStart(), act));
				tempAddList.add(new Edge(intersections.get(act)[1].getEnd(), act));
				tempRemoveList.add(newSeg);						
				tempRemoveList.add(intersections.get(act)[1]);
			}
		}
		
		mSegmentsList.removeAll(tempRemoveList);
		mSegmentsList.addAll(tempAddList);
			
		for(Point k : intersections.keySet()) {
			
			mPointsList.add(k);
			
		}
	}
	
	/**
	 * Zeichne Extensions
	 * @param a
	 * @param b
	 * @param paint
	 * @param width
	 * @return
	 */
	private Edge createUpperOrLowerExtension(Point a, Point b, Color paint, double width) {
		
		drawLine(mGc, a, b, COLOR_RED, LINE_SLAB_WIDTH);
		
		return new Edge(a, b);
		
	}
	
	/**
	 * Ermittle Schnittpunkte f�r vertikale Erweiterungen
	 * @param temp
	 * @return
	 */
	private ArrayList<Point> getIntersectionsForUpperAndLowerExtensions(Edge temp) {
		
		ArrayList<Point> list = new ArrayList<Point>();
		
		for(Edge t : mSegmentsList) {
						
			Point p = Helper.isIntersection(temp, t);
			
			if(null != p) {
				
				if((t.getStart().getX() < p.getX() && t.getEnd().getX() > p.getX())
												||
				   (t.getEnd().getX() < p.getX() && t.getStart().getX() > p.getX())) {
					
					list.add(p);
					
				}
			}
		}
		
		return list;
	}
	
	/**
	 * Ermittle n�chsten Schnittpunkt zu Segment
	 * @param list
	 * @param p
	 * @param upperOrLower
	 * @return
	 */
	private Point getNextIntersectionToSegment(ArrayList<Point> list, Point p, boolean upperOrLower) {
		
		double diff = Double.MAX_VALUE;
		
		Point poi = null;
		
		if(upperOrLower) {
			
			for(Point po : list) {
				
				if(po.getY() < p.getY()) {
					
					if((p.getY() - po.getY()) < diff) {
						
						diff = p.getY() - po.getY();
						poi = po;
						
					}
				}	
			}
			
		} else if(!upperOrLower){
			
			for(Point po : list) {
				
				if(po.getY() > p.getY()) {
					
					if((po.getY() - p.getY()) < diff) {
						
						diff = po.getY() - p.getY();
						poi = po;
						
					}
				}
			}
		}
		
		return poi;
	}
	
	
	/**
	 * Erzeuge obere und untere vertikale Erweiterungen f�r den Ansatz Trapezkarte
	 * @param e
	 */
	@FXML protected void createVerticalExtensions(ActionEvent e) {
		
		if(!mBorder) {
			
			createAlertMessage(AlertType.INFORMATION, TITLE_CAREFUL, MESSAGE_INITIALIZE);
			return;
		
		} else if (mSlabsCreated) {
			
			createAlertMessage(AlertType.INFORMATION, TITLE_CAREFUL, MESSAGE_SLABS_CREATED);
			return;
			
		}
		
		ArrayList<Point> intersections = new ArrayList<Point>();
		
		for(Point p : mPointsList) {
			
			Edge temp = new Edge(p, new Point(p.getX(), 0));
			intersections = getIntersectionsForUpperAndLowerExtensions(temp);
			Point poi = getNextIntersectionToSegment(intersections, p, true);
			
			if(poi==null) {
				
				Edge ex = createUpperOrLowerExtension(p, new Point(p.getX(),0), COLOR_RED, LINE_SLAB_WIDTH);
				p.setUpperExtension(ex);
				
			} else {
				
				Edge ex = createUpperOrLowerExtension(p, poi, COLOR_RED, LINE_SLAB_WIDTH);
				p.setUpperExtension(ex);
				
			}
			
			poi = getNextIntersectionToSegment(intersections, p, false);
			
			if(poi==null) {
				
				Edge ex = createUpperOrLowerExtension(p, new Point(p.getX(), mHeigth), COLOR_RED, LINE_SLAB_WIDTH);
				ex.getStart().setLowerExtension(ex);
				
			} else {
				
				Edge ex = createUpperOrLowerExtension(p, poi, COLOR_RED, LINE_SLAB_WIDTH);
				ex.getStart().setLowerExtension(ex);

			}
			
			
			intersections.clear();
			
		}
		
		mExtensionsCreated = true;
		
	}
	
	/**
	 * Erzeuge Trapezkarte
	 * @param e
	 */
	@FXML protected void createTrapecoidalMap(ActionEvent e) {
		
		if(!mBorder) {
			
			createAlertMessage(AlertType.INFORMATION, TITLE_CAREFUL, MESSAGE_INITIALIZE);
			return;
			
		} else if (!mExtensionsCreated) {
			
			createAlertMessage(AlertType.INFORMATION, TITLE_CAREFUL, MESSAGE_INITIALIZE_DATASTRUCTURE);
			return;
			
		}
		
		Collections.sort(mSegmentsList);
		
//		for(Point p : mPointsList) {
//			System.out.println(p.toString());
//		}
//		
//		for(Segment s: mSegmentsList) {
//			System.out.println(s.toString());
//		}

		
		ArrayList<Edge> removeList = new ArrayList<Edge>();
		
		for(int i = 0; i < mSegmentsList.size(); i++) {
			
			for(int j = i; j < mSegmentsList.size();j++) {
				
				if(i==j) {
					continue;
				}
				
				if(mSegmentsList.get(i).getStart().equals(mSegmentsList.get(j).getStart())
						&& mSegmentsList.get(i).getEnd().equals(mSegmentsList.get(j).getEnd())) {
					
					removeList.add(mSegmentsList.get(j));
					
				}
			}
		}
		
		mSegmentsList.removeAll(removeList);
		
		try {
			this.trapecoidalMapDatastructure.computeTrapecoidalMap(mSegmentsList);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		mTrapecoidalDatastructureCreated = true;
		
	}
	
	/**
	 * Helper-Methode; setzte Zeichenfl�che und alles damit zusammenh�ngende zur�ck
	 */
	private void clearPaintingAreaAndDatastructure() {
		
		segmentPointCounter = 0;
		mPoints[0] = null;
		mPoints[1] = null;
		mSegmentsList.clear();
		mPointsList.clear();
		mSlabList.clear();
		mSlabSegmentsList.clear();
		mExtensionSegmentsList.clear();
		trapecoidalMapStates.clear();
		mDebugViewIndex = 1;
		drawRectangle(mGc, COLOR_LIGHTGRAY, COLOR_GREEN, LINE_BOX_WIDTH, mWidth, mHeigth);
		drawRectangle(mGcDebug, COLOR_LIGHTGRAY, COLOR_GREEN, LINE_BOX_WIDTH, mWidth, mHeigth);
		mBorder = true;
		mSlabsCreated = false;
		mExtensionsCreated = false;
		mNaiveDatastructureCreated = false;
		mTrapecoidalDatastructureCreated = false;
		simpleApproachDatastructure = new SimpleApproachDataStructure(mWidth, mHeigth);
		trapecoidalMapDatastructure = new TrapecoidalMap(mWidth, mHeigth);
		trapecoidalMapDatastructure.addObserver(this);
		mRoot = new TreeItem<String>("Zust�nde");
		mRoot.getChildren().clear();
		mRoot.setExpanded(false);
		mTreeViewGraph.setRoot(mRoot);
		mLabelCounter.setText("1");
	}
	
	/**
	 * Linie zeichnen
	 * @param start
	 * @param end
	 * @param p
	 * @param lineWidth
	 */
	private void drawLine(GraphicsContext gc, Point start, Point end, Paint p, double lineWidth) {
		
		gc.beginPath();
		gc.setStroke(p);
		gc.setLineWidth(lineWidth);
		gc.moveTo(start.getX(), start.getY());
		gc.lineTo(end.getX(), end.getY());
		gc.stroke();
		gc.closePath();
		
	}
	
	/**
	 * Punkt zeichnen
	 * @param x
	 * @param y
	 * @param paintFill
	 * @param paintLine
	 * @param lineWidth
	 * @param radius
	 * @return
	 */
	private Point drawPoint(GraphicsContext gc, double x, double y, Paint paintFill, Paint paintLine, double lineWidth, double radius) {
		
		gc.beginPath();
		gc.moveTo(x, y);
		gc.setFill(paintFill);
		gc.setStroke(paintLine);
		gc.setLineWidth(lineWidth);
		gc.fillOval(x-radius, y-radius, radius*2, radius*2);
		gc.strokeOval(x-radius, y-radius, radius*2, radius*2);
		gc.closePath();
		return new Point(x,y);
		
	}
	
	/**
	 * Rechteck zeichnen
	 * @param fillColor
	 * @param strokeColor
	 * @param lineWidth
	 * @param rectWidth
	 * @param rectHeight
	 */
	private void drawRectangle(GraphicsContext gc, Paint fillColor, Paint strokeColor, double lineWidth, double rectWidth, double rectHeight) {
		
		gc.clearRect(0, 0, rectWidth, rectHeight);
		gc.setFill(fillColor);
		gc.setStroke(strokeColor);
		gc.setLineWidth(lineWidth);
		gc.fill();
		gc.strokeRect(0, 0, rectWidth, rectHeight);
		
	}
	
	/**
	 * Liefert true zur�ck, wenn die euklidische Distanz zwischen p1 und p2 kleiner EUCLIDIC_DISTANCE_GET_NEAREST_POINT ist
	 * @param p1
	 * @param p2
	 * @return
	 */
	private boolean checkDifference(Point p1, Point p2) {
		
		double c = Math.sqrt((p1.getX()-p2.getX())*(p1.getX()-p2.getX()) 
				   + (p1.getY()-p2.getY())*(p1.getY()-p2.getY()));
		
		if(c < EUCLIDIC_DISTANCE_GET_NEAREST_POINT) {
			
			return true;
			
		} else {
			
			return false;
			
		}
	}
	
	/**
	 * Ermittle den n�chsten Punkt zu den Koordinanten x und y, indem die euklidische Distanz berechnet wird
	 * @param x
	 * @param y
	 * @return
	 */
	private Point getNearestPoint(double x, double y) {
		
		double value = -1;
		
		Point temp = null;
		
		for(Point p : mPointsList) {
			
			double c = Math.sqrt((p.getX()-x)*(p.getX()-x) + (p.getY()-y)*(p.getY()-y));
			
			if(value == -1) {
				
				if(c < EUCLIDIC_DISTANCE_GET_NEAREST_POINT) {
					
					value = c;
					temp = p;
					
				}
				
			} else if (c < value) {
//				System.out.println("GetNearestPoint: c ist kleiner als Value " + c);
				value = c;
				temp = p;
				
			}
		}
		
		if(value == -1) {
			
			return null;
			
		} else {
			
			return temp;
			
		}
	}
	
	/**
	 * Erzeuge MessageBox
	 * @param type
	 * @param title
	 * @param contentText
	 */
	private void createAlertMessage(Alert.AlertType type, String title, String contentText) {
		
		Alert alert = new Alert(type);
		alert.setTitle(title);
		alert.setHeaderText(null);
		alert.setContentText(contentText);
		alert.showAndWait();
		
	}
	
	
	/**
	 * Programm beenden
	 */
	@FXML protected void close() {
		
		Platform.exit();
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public void update(Observable arg0,
			Object o) {
		
		trapecoidalMapStates = (HashMap<Integer, HashMap<ArrayList<Trapecoid>, DirectedAcyclicGraph>>) o;
		
		int count = trapecoidalMapStates.size();
		
//		trapecoidalMapStates.put(++count,(HashMap<ArrayList<Trapecoid>, DirectedAcyclicGraph>)o);
		
		ArrayList<Trapecoid> key = null;
		
		for(ArrayList<Trapecoid> k : trapecoidalMapStates.get(count).keySet()) {
			key = k;
		}
		
		SearchNode root = trapecoidalMapStates.get(count).get(key).getRoot();
		
		TreeItem<String> subRoot = new TreeItem<String>("Zustand "+count);

		TreeItem<String> graph = traverse(root, subRoot);
		
		mRoot.getChildren().add(graph);
		
		if(count==1) {
			paintDebugState();
		}
		
	}
	
	public TreeItem<String> traverse(SearchNode rootNode, TreeItem<String> rootItem) {
		
		TreeItem<String> item = new TreeItem<String>(rootNode.toString());
		TreeItem<String> left = null;
		TreeItem<String> right = null;
		
		if(rootNode.getLeftChild() == null) {
			left = new TreeItem<String>("Linker Nachbar ist null");
			item.getChildren().add(left);
		} else if (rootNode.getLeftChild() != null) {
			left = traverse(rootNode.getLeftChild(), item);
		}
		
		if(rootNode.getRightChild() == null) {
			right = new TreeItem<String>("Rechter Nachbar ist null");
			item.getChildren().add(right);
		} else if (rootNode.getRightChild() != null) {
			right = traverse(rootNode.getRightChild(), item);
		}
		
		rootItem.getChildren().add(item);
		
		return rootItem;
		
	}
	
	public void paintDebugState() {
		
		drawRectangle(mGcDebug, COLOR_LIGHTGRAY, COLOR_GREEN, LINE_BOX_WIDTH, mWidth, mHeigth);
		
		HashMap<ArrayList<Trapecoid>, DirectedAcyclicGraph> map = trapecoidalMapStates.get(mDebugViewIndex);
		
		ArrayList<Trapecoid> key = null;
		
		for(ArrayList<Trapecoid> t: map.keySet()) {
			
			key = t;
			
		}
		
		ArrayList<SearchNode> nodes = map.get(key).getSearchNodes();
		
		for(SearchNode n : nodes) {
			
			if(n instanceof Point) {
				
				drawPoint(mGcDebug, ((Point) n).getX(), ((Point) n).getY(), null, COLOR_RED, LINE_WIDTH, RADIUS_CIRCLE);
				drawLine(mGcDebug, ((Point) n).getUpperExtension().getStart(), ((Point) n).getUpperExtension().getEnd(), COLOR_RED, LINE_WIDTH);
				drawLine(mGcDebug, ((Point) n).getLowerExtension().getStart(), ((Point) n).getLowerExtension().getEnd(), COLOR_RED, LINE_WIDTH);
				
			} else if(n instanceof Edge) {
				
				drawLine(mGcDebug, ((Edge) n).getStart(), ((Edge) n).getEnd(), COLOR_BLUE, LINE_WIDTH);
				
			} 
//			else if(n instanceof Trapecoid) {
//				
//				Trapecoid t = (Trapecoid)n;
//				
//				double x = (t.getLeftp().getX() + t.getRightp().getX()) / 2;
//				double y = (t.getTop().getStart().getY() + t.getBottom().getStart().getY()) / 2;
//				
//				String text = t.getID()+"";
//				
////				drawText(x, y, text);
//			}
		}
	}
	
	protected void drawText(double x, double y, String text) {
		
		mGcDebug.strokeText(text, x, y);
		
	}
	
	@FXML protected void getPrevState(ActionEvent e) {
		int actualCounter = Integer.parseInt(mLabelCounter.getText());
		
		if(actualCounter==1) {
			return;
		} else {
			actualCounter--;
			mLabelCounter.setText(""+actualCounter);
			mDebugViewIndex--;
			paintDebugState();
		}
	}
	
	@FXML protected void getNextState(ActionEvent e) {
		int actualCounter = Integer.parseInt(mLabelCounter.getText());
		
		if(actualCounter == trapecoidalMapStates.size()) {
			return; 
		} else {
			actualCounter++;
			mLabelCounter.setText(""+actualCounter);
			mDebugViewIndex++;
			paintDebugState();
		}
	}
}
