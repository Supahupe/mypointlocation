package de.hupertz.mypointlocation.model;

/**
 * Strecke bzw. Kante
 * @author Julian
 *
 */
public class Segment extends SearchNode implements Comparable<Segment> {
	
	private Point start;
	private Point end;
	private Point middle;
	
	private double gradient;
	private double axis;
	
	public Segment (Point start, Point end) {
		
		if(start.getX() <= end.getX()) {
			this.start = start;
			this.end = end;
		} else {
			this.start = end;
			this.end = start;
		}
		
		gradient = (start.getY() - end.getY()) /  (start.getX() - end.getX());
		axis = start.getY() - gradient * start.getX();
		double middleX = (start.getX() + end.getX()) / 2;
		double middleY = gradient * middleX + axis;
		middle = new Point(middleX, middleY);
	}
	
	public Segment (Point start, Point middle, Point end) {
		this.setStart(start);
		this.setEnd(end);
		this.middle = middle;
		gradient = (start.getY() - end.getY()) /  (start.getX() - end.getX());
		axis = start.getY() - gradient * start.getX();
	}
	
	public Point getStart() {
		return start;
	}
	
	public void setStart(Point start) {
		this.start = start;
	}
	
	public Point getEnd() {
		return end;	
	}
	
	public Point getMiddle() {
		return middle;
	}
	
	public void setMiddle(Point middle) {
		this.middle = middle;
	}
	
	public void setEnd(Point end) {
		this.end = end;
	}
	
	public String getTextAreaDescription() {
		return "Segment:\n"
			  +"Punkt 1 X-Koordinate: " + this.getStart().getX() + "\n"
			  +"Punkt 1 Y-Koordinate: " + this.getStart().getY() + "\n"
			  +"Punkt 2 X-Koordinate: " + this.getEnd().getX() + "\n"
			  +"Punkt 2 Y-Koordinate: " + this.getEnd().getY() + "\n";
	}
	
	@Override
	public String toString() {
		return "Segment: P1 " + "(" + start.getX() + "," + start.getY() + ")" + 
					" P2 " + "(" + end.getX() + "," + end.getY() + ")";
	}

	@Override
	public int compareTo(Segment seg) {
		if(seg == null) 
			throw new NullPointerException();
		else if (this.middle.getY() == seg.middle.getY())
			return 0;
		else if (this.middle.getY() < seg.middle.getY())
			return -1;
		else
			return 1;
	}
	
	public double getAxis() {
		return axis;
	}

	public void setAxis(double axis) {
		this.axis = axis;
	}
	
	public double getGradient() {
		return gradient;
	}

	public void setGradient(double gradient) {
		this.gradient = gradient;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((end == null)
				? 0 : end.hashCode());
		result = prime * result
				+ ((middle == null) ? 0
						: middle.hashCode());
		result = prime * result + ((start == null)
				? 0 : start.hashCode());
		
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Segment other = (Segment) obj;
		if (end == null) {
			if (other.end != null)
				return false;
		} else if (!end.equals(other.end))
			return false;
		if (middle == null) {
			if (other.middle != null)
				return false;
		} else if (!middle.equals(other.middle))
			return false;
		if (start == null) {
			if (other.start != null)
				return false;
		} else if (!start.equals(other.start))
			return false;
		
		return true;
	}
}