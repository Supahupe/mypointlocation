package de.hupertz.mypointlocation.model;

import java.util.ArrayList;

import de.hupertz.mypointlocation.utils.Helper;

public class Slab implements Comparable<Slab> {
	private Segment leftSegment;
	private Segment rightSegment;
	private Segment middleSegment;
	private ArrayList<Face> faceList=null;
	
	public Slab (Segment leftSegment, Segment rightSegment) {
		if(leftSegment.getStart().getX() < rightSegment.getStart().getX()) {
			this.setLeftSegment(leftSegment);
			this.setRightSegment(rightSegment);
		} else {
			this.setLeftSegment(rightSegment);
			this.setRightSegment(leftSegment);
		}
		
		setMiddleSegment(new Segment(new Point((this.leftSegment.getStart().getX() 
				+ this.rightSegment.getStart().getX())/2, 0), 
				new Point((this.leftSegment.getStart().getX() 
						+ this.rightSegment.getStart().getX())/2, 
						this.leftSegment.getEnd().getY())));
	}
	
	public Face binarySearch(Point p) {
		
		int lowerbound = 0;
		int upperbound = faceList.size();
		int position = (lowerbound + upperbound) / 2;
		boolean flag = false;
		
		while(!flag) {
			
			//TODO: Optimieren, da Segmente auch "schief" sein k�nnen -> 
			//der angefragte Punkt muss eine Gerade bilden und
			//der Schnittpunkt mit dem LowerSegment muss gebildet werden -> 
			//dann kann die ABfrage durchgef�hrt werden
			Segment s = new Segment(new Point(p.getX(), 0), new Point(p.getX(), p.getY()));
			
			Point intersec1 = Helper.isIntersection(faceList.get(position).getLowerSegment(), s);
			Point intersec2 = Helper.isIntersection(faceList.get(position).getUpperSegment(), s);
			
			if((intersec1.getY() < p.getY()) && (intersec2.getY() < p.getY()))
				lowerbound = position + 1;
			else if((intersec1.getY() > p.getY()) && (intersec2.getY() > p.getY()))
				upperbound = position - 1;
			else 
				flag = true;
			
			if(!flag)
				position = (lowerbound + upperbound) / 2;
		}
		
		return faceList.get(position);
	}
	
	public void addFace(Face f) {
		if(faceList == null) 
			faceList = new ArrayList<Face>();
		faceList.add(f);
	}
	
	public Segment getLeftSegment() {
		return leftSegment;
	}

	public void setLeftSegment(Segment leftSegment) {
		this.leftSegment = leftSegment;
	}
	
	public Segment getRightSegment() {
		return rightSegment;
	}

	public void setRightSegment(Segment rightSegment) {
		this.rightSegment = rightSegment;
	}
	

	public ArrayList<Face> getFaceList() {
		return faceList;
	}

	public void setFaceList(ArrayList<Face> faceList) {
		this.faceList = faceList;
	}
	
	public Segment getMiddleSegment() {
		return middleSegment;
	}

	private void setMiddleSegment(Segment middleSegment) {
		this.middleSegment = middleSegment;
	}
	
	@Override
	public String toString() {
		return "Linker Abschnitt: " 
			+ "(" + this.leftSegment.getStart().getX() + " , " 
				+ this.leftSegment.getStart().getY() + ")" 
			+ "(" + this.leftSegment.getEnd().getX() + " , " 
				+ this.leftSegment.getEnd().getY() + ")"
			+ "\n"
			+ "Mittlerer Abschnitt: "
			+ "(" + this.middleSegment.getStart().getX() + " , " 
				+ this.middleSegment.getStart().getY() + ")"
			+ "(" + this.middleSegment.getEnd().getX() + " , " 
				+ this.middleSegment.getEnd().getY() + ")"
			+ "\n"
			+ "Rechter Abschnitt: " 
			+ "(" + this.rightSegment.getStart().getX() + " , " 
				+ this.rightSegment.getStart().getY() + ")"
			+ "(" + this.rightSegment.getEnd().getX() + " , " 
				+ this.rightSegment.getEnd().getY() + ")" +"\n";
	}

	@Override
	public int compareTo(Slab s) {
		if (s==null)		
			throw new NullPointerException();
		else if(this.leftSegment.getStart().getX() == s.getLeftSegment().getStart().getX())
			return 0;
		else if(this.leftSegment.getStart().getX() < s.getLeftSegment().getStart().getX()) 
			return -1;
		else
			return 1;
	}
}
