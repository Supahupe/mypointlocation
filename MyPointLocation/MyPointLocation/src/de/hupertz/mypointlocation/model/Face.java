package de.hupertz.mypointlocation.model;

/**
 * Dummy-Class f�r die einzelnen Scheiben-Segmente
 * @author Julian
 *
 */
public class Face {
	
	private int ID;
	private Segment upperSegment;
	private Segment lowerSegment;
	
	public Face (int id, Segment upper, Segment lower) {
		this.ID = id;
		this.setUpperSegment(upper);
		this.setLowerSegment(lower);
	}
	
	public int getID() {
		return ID;
	}
	
	public void setID(int iD) {
		ID = iD;
	}

	public Segment getUpperSegment() {
		return upperSegment;
	}

	public void setUpperSegment(Segment upperSegment) {
		this.upperSegment = upperSegment;
	}

	public Segment getLowerSegment() {
		return lowerSegment;
	}

	public void setLowerSegment(Segment lowerSegment) {
		this.lowerSegment = lowerSegment;
	}
	
	@Override
	public String toString() {
		return ID + " " + upperSegment.toString() + " " + lowerSegment.toString();
	}
}