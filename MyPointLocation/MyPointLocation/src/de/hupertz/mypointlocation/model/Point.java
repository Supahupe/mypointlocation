package de.hupertz.mypointlocation.model;

/**
 * Punkt bzw. Knoten
 * @author Julian
 *
 */
public class Point extends SearchNode implements Comparable<Point> {
	
	@SuppressWarnings("unused")
	private static final long serialVersionUID = -7021945634927969437L;
	private double x;
	private double y;
	private Segment upperExtension;
	private Segment lowerExtension;
	
	public Point(double x, double y) {
		this.x = Math.round(x * 1000)/1000;
		this.y = Math.round(y * 1000)/1000;
	}
	
	public Point(Point p) {
		this.x = p.getX();
		this.y = p.getY();
		this.upperExtension = p.getUpperExtension();
		this.lowerExtension = p.getLowerExtension();
	}

	public double getX() {
		return x;
	}
	
	public void setX(double x) {
		this.x = x;
	}
	
	public double getY() {
		return y;
	}
	
	public void setY(double y) {
		this.y = y;
	}
	
	@Override
	public String toString() {
		return "P: "+x+","+y +" ";
	}
	
	//Sortier-Methode um lexikographischen Vergleich zu ermöglichen
	@Override
	public int compareTo(Point p) {
		
		if (p == null)
			throw new NullPointerException();
		else if (this.getX() == p.getX())
			if(this.getY() < p.getY())
				return -1;
			else if (this.getY() > p.getY())
				return 1;
			else
				return 0;
		else if (this.getX() < p.getX())
			return -1;	
		else
			return 1;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result
				+ (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result
				+ (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Point other = (Point) obj;
		if (Double.doubleToLongBits(x) != Double
				.doubleToLongBits(other.x))
			return false;
		if (Double.doubleToLongBits(y) != Double
				.doubleToLongBits(other.y))
			return false;
		return true;
	}

	public Segment getUpperExtension() {
		return upperExtension;
	}

	public void setUpperExtension(Segment upperExtension) {
		this.upperExtension = upperExtension;
	}

	public Segment getLowerExtension() {
		return lowerExtension;
	}

	public void setLowerExtension(
			Segment lowerExtension) {
		this.lowerExtension = lowerExtension;
	}
	
	public String getTextAreaDescription() {
		
		return "Punkt:\n"
			  +"X-Koordinate: " + this.getX() + "\n"
			  +"Y-Koordinate: " + this.getY() + "\n";
	}
}
