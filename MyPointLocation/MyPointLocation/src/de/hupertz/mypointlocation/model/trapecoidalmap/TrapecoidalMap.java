package de.hupertz.mypointlocation.model.trapecoidalmap;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Observable;
import java.util.Random;

import de.hupertz.mypointlocation.model.Point;
import de.hupertz.mypointlocation.model.SearchNode;
import de.hupertz.mypointlocation.model.Segment;
import de.hupertz.mypointlocation.utils.Helper;


/**
 * Datenstruktur f�r Trapezkarten-Algorithmus
 * @author Julian
 *
 */
public class TrapecoidalMap extends Observable implements Serializable {
	
	private static final long serialVersionUID = -518107651841640744L;
	private ArrayList<Trapecoid> trapecoidList;
	private DirectedAcyclicGraph searchGraph;
	HashMap<Integer, HashMap<ArrayList<Trapecoid>, DirectedAcyclicGraph>> map;	
	int elemCounter = 1;
	double mHeight = -1;
	double mWidth = -1;

	/**
	 * Konstruktor der Klasse f�r den Trapezkarten-Algorithmus und die dazugeh�rigen Datenstrukturen
	 * @param mWidth
	 * @param mHeight
	 */
	public TrapecoidalMap(double mWidth, double mHeight) {
		this.mHeight = mHeight;
		this.mWidth = mWidth;
		this.map = new HashMap<Integer, HashMap<ArrayList<Trapecoid>, DirectedAcyclicGraph>>();
		trapecoidList = new ArrayList<Trapecoid>();
		Trapecoid t = new Trapecoid(0, 0, mHeight, mWidth);
		t.setParent(null);
		t.setLeftChild(null);
		t.setRightChild(null);
		trapecoidList.add(t);
		searchGraph = new DirectedAcyclicGraph();
		searchGraph.addNode(t);
	}
	
	/**
	 * AKtualisiere Nachbarschaften
	 * TODO: Methode muss �berarbeitet werden
	 * @throws Exception
	 */
	public void updateNeighbourships() throws Exception {
	
		for(Trapecoid t : trapecoidList) {
			
			for(Trapecoid u : trapecoidList) {
				
				if(t==u) 
					continue;
							
				if(t.getTop() == u.getTop()) {
											
						if(t.rightDeg == true || u.leftDeg == true) {
							t.setUpperRightAdjacentTrapecoid(u);
							u.setUpperLeftAdjacentTrapecoid(t);
						}
						
						if(u.rightDeg == true || t.leftDeg == true) {
							u.setUpperRightAdjacentTrapecoid(t);
							t.setUpperLeftAdjacentTrapecoid(u);
					}
				} 
				
				if (t.getBottom() == u.getBottom()) {
					
					if(t.getRightp().getX() == u.getLeftp().getX()) {
						
						t.setLowerRightAdjacentTrapecoid(u);
						u.setLowerLeftAdjacentTrapecoid(t);
						
					} else if(t.getLeftp().getX() == u.getRightp().getX()) {
						
						u.setLowerRightAdjacentTrapecoid(t);
						t.setLowerLeftAdjacentTrapecoid(u);
						
					}
				} 	
			}
		}
	}
	
	/**
	 * Berechne Trapezkarte und dazugeh�rige Datenstruktur
	 * TODO: Ermittlung der Nachbarschaften funktioniert noch nicht sauber
	 * @param segments
	 * @throws Exception
	 */
	public void computeTrapecoidalMap(ArrayList<Segment> segments) throws Exception {
		
		if(segments.size() == 0)
			return;
		
		Collections.sort(segments);
		
		for(Segment s : segments) {
			
			ArrayList<Trapecoid> list = intersectedTrapecoids(s);
			
			if(list.size() == 1) { //Fall, dass nur ein Trapez geschnitten wird ; Segment liegt in einem Trapez
				
				//Startpunkt des Segments liegt am linken Rand des geschnittenen Trapezes
				//list.get(0).getLeftp().getX() == s.getStart().getX()) && (list.get(0).getRightp().getX() != s.getEnd().getX()
				if((list.get(0).getLeftp().getX() == s.getStart().getX()) && 
						(list.get(0).getRightp().getX() != s.getEnd().getX())) {
					
					Trapecoid t1 = new Trapecoid(list.get(0).getTop(), 
							s, s.getStart(), s.getEnd());
					Trapecoid t2 = new Trapecoid(s, list.get(0).getBottom(), 
							s.getStart(), s.getEnd());
					Trapecoid t3 = new Trapecoid(list.get(0).getTop(), 
							list.get(0).getBottom(), s.getEnd(), list.get(0).getRightp());
					
					t1.setUpperLeftAdjacentTrapecoid(list.get(0).getUpperLeftAdjacentTrapecoid());
					t1.setUpperRightAdjacentTrapecoid(t3);
					t2.setLowerLeftAdjacentTrapecoid(list.get(0).getLowerLeftAdjacentTrapecoid());
					t2.setLowerRightAdjacentTrapecoid(t3);
					t3.setUpperLeftAdjacentTrapecoid(t1);
					t3.setLowerLeftAdjacentTrapecoid(t2);
					t3.setUpperRightAdjacentTrapecoid(list.get(0).getUpperRightAdjacentTrapecoid());
					t3.setLowerRightAdjacentTrapecoid(list.get(0).getLowerRightAdjacentTrapecoid());
					
					if(list.get(0).getUpperLeftAdjacentTrapecoid() != null)
						list.get(0).getUpperLeftAdjacentTrapecoid().
							setUpperRightAdjacentTrapecoid(t1);
					
					if(list.get(0).getLowerLeftAdjacentTrapecoid() != null)
						list.get(0).getLowerLeftAdjacentTrapecoid().
							setLowerRightAdjacentTrapecoid(t2);
					
					if(list.get(0).getUpperRightAdjacentTrapecoid() != null)
						list.get(0).getUpperRightAdjacentTrapecoid().
							setUpperLeftAdjacentTrapecoid(t3);
					
					if(list.get(0).getLowerRightAdjacentTrapecoid() != null)
						list.get(0).getLowerRightAdjacentTrapecoid().
							setLowerLeftAdjacentTrapecoid(t3);
					
					SearchNode node = null;
					
					try {
						node = this.update(list.get(0), new Point (s.getEnd()));
						((Point)node).setUpperExtension(s.getEnd().getUpperExtension());
						((Point)node).setLowerExtension(s.getEnd().getLowerExtension());
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					node.setRightChild(t3);
					t3.setParent(node);
					node.setParent(node);
					Segment newS = new Segment(s.getStart(), s.getEnd());
					node.setLeftChild(newS);
					newS.setParent(node);
					newS.setLeftChild(t1);
					newS.setRightChild(t2);
					t1.setParent(newS);
					t2.setParent(newS);
					trapecoidList.add(t1);
					trapecoidList.add(t2);
					trapecoidList.add(t3);
					searchGraph.addNode(newS);
					searchGraph.addNode(t1);
					searchGraph.addNode(t2);
					searchGraph.addNode(t3);
					trapecoidList.remove(list.get(0));
					
				} 
				//Endpunkt des Segments liegt am rechten Rand des geschnittenen Trapezes
				//list.get(0).getLeftp().getX() != s.getStart().getX()) && (list.get(0).getRightp().getX() == s.getEnd().getX()
				else if(((list.get(0).getLeftp().getX() != s.getStart().getX()) && 
						(list.get(0).getRightp().getX() == s.getEnd().getX()))) {
					
					Trapecoid t1 = new Trapecoid(list.get(0).getTop(), 
							list.get(0).getBottom(), list.get(0).getLeftp(), s.getStart());
					Trapecoid t2 = new Trapecoid(list.get(0).getTop(), 
							s, s.getStart(), s.getEnd());
					Trapecoid t3 = new Trapecoid(s, list.get(0).getBottom(), 
							s.getStart(), s.getEnd());
					
					if(list.get(0).getUpperLeftAdjacentTrapecoid() != null)
						list.get(0).getUpperLeftAdjacentTrapecoid().
							setUpperRightAdjacentTrapecoid(t1);
					
					if(list.get(0).getLowerLeftAdjacentTrapecoid() != null)
						list.get(0).getLowerLeftAdjacentTrapecoid().
							setLowerRightAdjacentTrapecoid(t1);
					
					if(list.get(0).getUpperRightAdjacentTrapecoid() != null)
						list.get(0).getUpperRightAdjacentTrapecoid().
							setUpperLeftAdjacentTrapecoid(t2);
					
					if(list.get(0).getLowerRightAdjacentTrapecoid() != null)
						list.get(0).getLowerRightAdjacentTrapecoid().
							setLowerLeftAdjacentTrapecoid(t3);
					
					t1.setUpperLeftAdjacentTrapecoid(list.get(0).getUpperLeftAdjacentTrapecoid());
					t1.setLowerLeftAdjacentTrapecoid(list.get(0).getLowerLeftAdjacentTrapecoid());
					t1.setUpperRightAdjacentTrapecoid(t2);
					t1.setLowerRightAdjacentTrapecoid(t3);
					t2.setUpperLeftAdjacentTrapecoid(t1);
					t2.setUpperRightAdjacentTrapecoid(list.get(0).getUpperRightAdjacentTrapecoid());
					t3.setLowerLeftAdjacentTrapecoid(t1);
					t3.setLowerRightAdjacentTrapecoid(list.get(0).getLowerRightAdjacentTrapecoid());
					SearchNode node = null;
					
					try {
						node = this.update(list.get(0), new Point(s.getStart()));
						((Point)node).setUpperExtension(s.getStart().getUpperExtension());
						((Point)node).setLowerExtension(s.getStart().getLowerExtension());
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					node.setLeftChild(t1);
					t1.setParent(node);
					Segment newS = new Segment(s.getStart(), s.getEnd());
					node.setRightChild(newS);
					newS.setParent(node);
					newS.setLeftChild(t2);
					newS.setRightChild(t3);
					t2.setParent(newS);
					t3.setParent(newS);
					trapecoidList.add(t1);
					trapecoidList.add(t2);
					trapecoidList.add(t3);
					searchGraph.addNode(newS);
					searchGraph.addNode(t1);
					searchGraph.addNode(t2);
					searchGraph.addNode(t3);
					trapecoidList.remove(list.get(0));
					
				} 
				//Start- und Endpunkt des Segments liegen an den R�ndern des Trapezes
				//(list.get(0).getLeftp().getX() == s.getStart().getX()) && (list.get(0).getRightp().getX() == s.getEnd().getX())
				else if((list.get(0).getLeftp().getX() == s.getStart().getX()) && 
						(list.get(0).getRightp().getX() == s.getEnd().getX())) {

					Trapecoid t1 = new Trapecoid(list.get(0).getTop(), s, 
							s.getStart(), s.getEnd());
					Trapecoid t2 = new Trapecoid(s, list.get(0).getBottom(), 
							s.getStart(), s.getEnd());
					
					if(list.get(0).getUpperLeftAdjacentTrapecoid() != null)
						list.get(0).getUpperLeftAdjacentTrapecoid().
							setUpperRightAdjacentTrapecoid(t1);
					
					if(list.get(0).getLowerLeftAdjacentTrapecoid() != null)
						list.get(0).getLowerLeftAdjacentTrapecoid().
							setLowerRightAdjacentTrapecoid(t2);
					
					if(list.get(0).getUpperRightAdjacentTrapecoid() != null) {
						list.get(0).getUpperRightAdjacentTrapecoid().setUpperLeftAdjacentTrapecoid(t1);
					}
					
					if(list.get(0).getLowerRightAdjacentTrapecoid() != null) {
						list.get(0).getLowerRightAdjacentTrapecoid().setLowerLeftAdjacentTrapecoid(t2);
					}
					
					t1.setUpperLeftAdjacentTrapecoid(list.get(0).getUpperLeftAdjacentTrapecoid());
					t1.setUpperRightAdjacentTrapecoid(list.get(0).getUpperRightAdjacentTrapecoid());
					t2.setLowerLeftAdjacentTrapecoid(list.get(0).getLowerLeftAdjacentTrapecoid());
					t2.setLowerRightAdjacentTrapecoid(list.get(0).getLowerRightAdjacentTrapecoid());
					
					
					SearchNode node = null;
					
					Segment newS = new Segment(s.getStart(), s.getEnd());
					
					try {
						
						node = this.update(list.get(0), newS);
						
					} catch (Exception e) {
						
						e.printStackTrace();
						
					}
					
					node.setLeftChild(t1);
					t1.setParent(node);
					node.setRightChild(t2);
					t2.setParent(node);
					
					trapecoidList.add(t1);
					trapecoidList.add(t2);
					searchGraph.addNode(t1);
					searchGraph.addNode(t2);
					
					trapecoidList.remove(list.get(0));
					
//					updateNeighbourships();
					
				} 
				//Das Segment liegt vollst�ndig innerhalb des Trapezes
				else {
					
					Trapecoid t1 = new Trapecoid(list.get(0).getTop(), list.get(0).getBottom(), list.get(0).getLeftp(), s.getStart());
					Trapecoid t2 = new Trapecoid(list.get(0).getTop(), s, s.getStart(), s.getEnd());
					Trapecoid t3 = new Trapecoid(s, list.get(0).getBottom(), s.getStart(), s.getEnd());
					Trapecoid t4 = new Trapecoid(list.get(0).getTop(), list.get(0).getBottom(), s.getEnd(), list.get(0).getRightp());
					
					if(list.get(0).getUpperLeftAdjacentTrapecoid() != null) {
						list.get(0).getUpperLeftAdjacentTrapecoid().setUpperRightAdjacentTrapecoid(t1);
					}
					
					if(list.get(0).getLowerLeftAdjacentTrapecoid() != null) {
						list.get(0).getLowerLeftAdjacentTrapecoid().setLowerRightAdjacentTrapecoid(t1);
					}
					
					if(list.get(0).getUpperRightAdjacentTrapecoid() != null) {
						list.get(0).getUpperRightAdjacentTrapecoid().setUpperLeftAdjacentTrapecoid(t4);
					}
					
					if(list.get(0).getLowerRightAdjacentTrapecoid() != null) {
						list.get(0).getLowerRightAdjacentTrapecoid().setLowerLeftAdjacentTrapecoid(t4);
					}
					
					t1.setUpperLeftAdjacentTrapecoid(list.get(0).getUpperLeftAdjacentTrapecoid());
					t1.setLowerLeftAdjacentTrapecoid(list.get(0).getLowerLeftAdjacentTrapecoid());
					t1.setUpperRightAdjacentTrapecoid(t2);
					t1.setLowerRightAdjacentTrapecoid(t3);
					
					t2.setUpperLeftAdjacentTrapecoid(t1);
					t2.setUpperRightAdjacentTrapecoid(t4);
					
					t3.setLowerLeftAdjacentTrapecoid(t1);
					t3.setLowerRightAdjacentTrapecoid(t4);
					
					t4.setUpperLeftAdjacentTrapecoid(t2);
					t4.setLowerLeftAdjacentTrapecoid(t3);
					t4.setUpperRightAdjacentTrapecoid(list.get(0).getUpperRightAdjacentTrapecoid());
					t4.setLowerRightAdjacentTrapecoid(list.get(0).getLowerRightAdjacentTrapecoid());
					
					SearchNode node= null;
					
					try {
						
						node = this.update(list.get(0), new Point(s.getStart()));
						((Point)node).setUpperExtension(s.getStart().getUpperExtension());
						((Point)node).setLowerExtension(s.getStart().getLowerExtension());
						
					} catch (Exception e) {
						
						e.printStackTrace();
						
					}
					
					node.setLeftChild(t1);
					t1.setParent(node);
					
					SearchNode node2 = new Point(s.getEnd());
					((Point)node2).setUpperExtension(s.getEnd().getUpperExtension());
					((Point)node2).setLowerExtension(s.getEnd().getLowerExtension());
					
					node.setRightChild(node2);
					node2.setParent(node);
					
					Segment newS = new Segment(s.getStart(), s.getEnd());
					
					node2.setRightChild(t4);
					node2.setLeftChild(newS);
					t4.setParent(node2);
					newS.setParent(node2);
					
					newS.setLeftChild(t2);
					newS.setRightChild(t3);
					t2.setParent(newS);
					t3.setParent(newS);
					
					trapecoidList.add(t1);
					trapecoidList.add(t2);
					trapecoidList.add(t3);
					trapecoidList.add(t4);
					searchGraph.addNode(t1);
					searchGraph.addNode(t2);
					searchGraph.addNode(t3);
					searchGraph.addNode(t4);
					searchGraph.addNode(node2);
					searchGraph.addNode(newS);
					
					trapecoidList.remove(list.get(0));
					
//					updateNeighbourships();
				}
				
				// Fall list.size() ist gr��er als 1
			} else if(list.size() > 1) { 
				
				for(int i=0;i<list.size(); i++) {
					
					if(i==0) {
						
						if(s.getStart().getX() == list.get(i).getLeftp().getX()) { //Fall, dass p gleich leftp
								
							Trapecoid t1 = null;
							Trapecoid t2 = null;
							
//							if(list.get(i).getRightp().getX() < s.getEnd().getX()) {
							if(list.get(i).getTop().getEnd().getX() < list.get(i).getBottom().getEnd().getX()) {
								
								t1 = new Trapecoid(list.get(i).getTop(), s, list.get(i).getLeftp(), list.get(i).getRightp());
								t2 = new Trapecoid(s, list.get(i).getBottom(), list.get(i).getLeftp(), s.getEnd());
								
								if(list.get(i).getUpperRightAdjacentTrapecoid() != null) {
									
									list.get(i).getUpperRightAdjacentTrapecoid().setUpperLeftAdjacentTrapecoid(t1);
									t1.setUpperRightAdjacentTrapecoid(list.get(i).getUpperRightAdjacentTrapecoid());
								}
								
							} else if (list.get(i).getTop().getEnd().getX() > list.get(i).getBottom().getEnd().getX()) {
								
								t1 = new Trapecoid(list.get(i).getTop(), s, list.get(i).getLeftp(), s.getEnd());
								t2 = new Trapecoid(s, list.get(i).getBottom(), list.get(i).getLeftp(), list.get(i).getRightp());
								
								if(list.get(i).getLowerRightAdjacentTrapecoid() != null) {
									
									list.get(i).getLowerRightAdjacentTrapecoid().setLowerLeftAdjacentTrapecoid(t2);
									t2.setLowerRightAdjacentTrapecoid(list.get(i).getLowerRightAdjacentTrapecoid());
								}
								
							} else if (list.get(i).getTop().getEnd().getX() == list.get(i).getBottom().getEnd().getX()) {
								
								// Wir brauchen die Intersection zwischen einer Extension von Rightp und s
								
								Point interSec = Helper.isIntersection(s, list.get(i).getRightp().getLowerExtension());
								
								if(interSec.getY() > list.get(i).getRightp().getY()) {
									
									t1 = new Trapecoid(list.get(i).getTop(), s, list.get(i).getLeftp(), list.get(i).getRightp());
									t2 = new Trapecoid(s, list.get(i).getBottom(), list.get(i).getLeftp(), s.getEnd());
									
									if(list.get(i).getUpperRightAdjacentTrapecoid() != null) {
										
										list.get(i).getUpperRightAdjacentTrapecoid().setUpperLeftAdjacentTrapecoid(t1);
										t1.setUpperRightAdjacentTrapecoid(list.get(i).getUpperRightAdjacentTrapecoid());
									}
									
								} else {
									
									t1 = new Trapecoid(list.get(i).getTop(), s, list.get(i).getLeftp(), s.getEnd());
									t2 = new Trapecoid(s, list.get(i).getBottom(), list.get(i).getLeftp(), list.get(i).getRightp());
									
									if(list.get(i).getLowerRightAdjacentTrapecoid() != null) {
										
										list.get(i).getLowerRightAdjacentTrapecoid().setLowerLeftAdjacentTrapecoid(t2);
										t2.setLowerRightAdjacentTrapecoid(list.get(i).getLowerRightAdjacentTrapecoid());
									}
									
								}
							}

							if(list.get(i).getUpperLeftAdjacentTrapecoid() != null) {
								
								if(list.get(i).getUpperLeftAdjacentTrapecoid().getTop() == t1.getTop()) {
									
									list.get(i).getUpperLeftAdjacentTrapecoid().setUpperRightAdjacentTrapecoid(t1);
									t1.setUpperLeftAdjacentTrapecoid(list.get(i).getUpperLeftAdjacentTrapecoid());
																		
								}
								
							} 
							
							if(list.get(i).getLowerLeftAdjacentTrapecoid() != null) {
								
								
								if(list.get(i).getLowerLeftAdjacentTrapecoid().getBottom() == t2.getBottom()) {
									
									list.get(i).getLowerLeftAdjacentTrapecoid().setLowerRightAdjacentTrapecoid(t2);
									t2.setLowerLeftAdjacentTrapecoid(list.get(i).getLowerLeftAdjacentTrapecoid());
									
								}
								
							} 
							
							list.get(i+1).setNewUpperLeftAdjacentTrapecoid(t1);
							list.get(i+1).setNewLowerLeftAdjacentTrapecoid(t2);
								
							SearchNode node = null;
							
							Segment newS = new Segment(s.getStart(), s.getEnd());
							
							try {
								
								node = this.update(list.get(i), newS);
								
							} catch (Exception e) {
								
								e.printStackTrace();
								
							}
							
							t1.setParent(node);
							t2.setParent(node);
							node.setLeftChild(t1);
							node.setRightChild(t2);
							
							trapecoidList.add(t1);
							trapecoidList.add(t2);
							searchGraph.addNode(t1);
							searchGraph.addNode(t2);
							trapecoidList.remove(list.get(i));
							
//							updateNeighbourships();
								
						} else { //in diesem Fall werden 3 Trapeze erzeugt; s.getStart liegt rechts von leftp
															
							Trapecoid t1 = null;
							Trapecoid t2 = null;
							Trapecoid t3 = null;
															
							t1 = new Trapecoid(list.get(i).getTop(), list.get(i).getBottom(), list.get(i).getLeftp(), s.getStart());
							//
							if(list.get(i).getTop().getEnd().getX() < list.get(i).getBottom().getEnd().getX()) {
								
								t2 = new Trapecoid(list.get(i).getTop(), s, s.getStart(), list.get(i).getRightp());
								t3 = new Trapecoid(s, list.get(i).getBottom(), s.getStart(), s.getEnd());
								
								if(list.get(i).getUpperRightAdjacentTrapecoid() != null) {
									
									list.get(i).getUpperRightAdjacentTrapecoid().setUpperLeftAdjacentTrapecoid(t2);
									t2.setUpperRightAdjacentTrapecoid(list.get(i).getUpperRightAdjacentTrapecoid());
								}
								
							} else if(list.get(i).getTop().getEnd().getX() > list.get(i).getBottom().getEnd().getX()) {
								
								t2 = new Trapecoid(list.get(i).getTop(), s, s.getStart(), s.getEnd());
								t3 = new Trapecoid(s, list.get(i).getBottom(), s.getStart(), list.get(i).getRightp());
								
								if(list.get(i).getLowerRightAdjacentTrapecoid() != null) {
									
									list.get(i).getLowerRightAdjacentTrapecoid().setLowerLeftAdjacentTrapecoid(t3);
									t3.setLowerRightAdjacentTrapecoid(list.get(i).getLowerRightAdjacentTrapecoid());
								}
								
							} else if(list.get(i).getTop().getEnd().getX() == list.get(i).getBottom().getEnd().getX()) {
								
								//Wir brauchen den Schnittpunkt zwischen einer Extension von Rightp und s
								Point interSec = Helper.isIntersection(s, list.get(i).getRightp().getLowerExtension());
								
								if(interSec.getY() > list.get(i).getRightp().getY()) {
									
									t2 = new Trapecoid(list.get(i).getTop(), s, s.getStart(), list.get(i).getRightp());
									t3 = new Trapecoid(s, list.get(i).getBottom(), s.getStart(), s.getEnd());
									
									if(list.get(i).getUpperRightAdjacentTrapecoid() != null) {
										
										list.get(i).getUpperRightAdjacentTrapecoid().setUpperLeftAdjacentTrapecoid(t2);
										t2.setUpperRightAdjacentTrapecoid(list.get(i).getUpperRightAdjacentTrapecoid());
									}
									
								} else {
									
									t2 = new Trapecoid(list.get(i).getTop(), s, s.getStart(), s.getEnd());
									t3 = new Trapecoid(s, list.get(i).getBottom(), s.getStart(), list.get(i).getRightp());
									
									if(list.get(i).getLowerRightAdjacentTrapecoid() != null) {
										
										list.get(i).getLowerRightAdjacentTrapecoid().setLowerLeftAdjacentTrapecoid(t3);
										t3.setLowerRightAdjacentTrapecoid(list.get(i).getLowerRightAdjacentTrapecoid());
									}
									
								}
								
							}
							
							if(list.get(i).getUpperLeftAdjacentTrapecoid() != null) {
								
								if(list.get(i).getUpperLeftAdjacentTrapecoid().getTop() == t1.getTop()) {
										
									list.get(i).getUpperLeftAdjacentTrapecoid().setUpperRightAdjacentTrapecoid(t1);
									t1.setUpperLeftAdjacentTrapecoid(list.get(i).getUpperLeftAdjacentTrapecoid());

								}
									
							} 
								
							if(list.get(i).getLowerLeftAdjacentTrapecoid() != null) {
									
								if(list.get(i).getLowerLeftAdjacentTrapecoid().getBottom() == t1.getBottom()) {
										
									list.get(i).getLowerLeftAdjacentTrapecoid().setLowerRightAdjacentTrapecoid(t1);
									t1.setLowerLeftAdjacentTrapecoid(list.get(i).getLowerLeftAdjacentTrapecoid());
									
								}
									
							} 
							
							list.get(i+1).setNewUpperLeftAdjacentTrapecoid(t2);
							list.get(i+1).setNewLowerLeftAdjacentTrapecoid(t3);

							t1.setLowerRightAdjacentTrapecoid(t3);
							t1.setUpperRightAdjacentTrapecoid(t2);
							t2.setUpperLeftAdjacentTrapecoid(t1);
							t3.setLowerLeftAdjacentTrapecoid(t1);
							
							SearchNode node = null;
							
							try {
								node = this.update(list.get(i), s.getStart());
								((Point)node).setUpperExtension(s.getStart().getUpperExtension());
								((Point)node).setLowerExtension(s.getStart().getLowerExtension());
								
							} catch (Exception e) {
								e.printStackTrace();
							}
								
							t1.setParent(node);
							node.setLeftChild(t1);
							
							Segment newS = new Segment(s.getStart(), s.getEnd());
							node.setRightChild(newS);
							newS.setParent(node);
													
							t2.setParent(newS);
							t3.setParent(newS);
							newS.setLeftChild(t2);
							newS.setRightChild(t3);

							trapecoidList.add(t1);
							trapecoidList.add(t2);
							trapecoidList.add(t3);
							searchGraph.addNode(t1);
							searchGraph.addNode(t2);
							searchGraph.addNode(t3);
							searchGraph.addNode(newS);
							trapecoidList.remove(list.get(i));
							
//							updateNeighbourships();
						}
						
					} else if(i == list.size() - 1) { 
						
						if(s.getEnd().getX()==list.get(i).getRightp().getX()) {
								
							//Fall, dass p gleich rightp
							Trapecoid prevUpper = list.get(i).getNewUpperLeftAdjacentTrapecoid();
							Trapecoid prevLower = list.get(i).getNewLowerLeftAdjacentTrapecoid();
							
							Trapecoid t1 = null;
							Trapecoid t2 = null;
							
							if(prevUpper.getTop() == list.get(i).getTop()) {
								
								prevUpper.setRightp(list.get(i).getRightp());
								
							} else if (prevLower.getBottom() == list.get(i).getBottom()) {
								
								prevLower.setRightp(list.get(i).getRightp());
								
							}
							
							if(prevLower.getRightp().getX() == s.getEnd().getX()) {
								
								t1 = new Trapecoid(list.get(i).getTop(), s, list.get(i).getLeftp(), list.get(i).getRightp());
								t2 = prevLower;
								
								prevUpper.setLowerRightAdjacentTrapecoid(t1);
								t1.setLowerLeftAdjacentTrapecoid(prevUpper);
								
								if(list.get(i).getUpperLeftAdjacentTrapecoid()!=null) {
									list.get(i).getUpperLeftAdjacentTrapecoid().setUpperRightAdjacentTrapecoid(t1);
									t1.setUpperLeftAdjacentTrapecoid(list.get(i).getUpperLeftAdjacentTrapecoid());
								}
								
								if(list.get(i).getUpperRightAdjacentTrapecoid() != null) {
									list.get(i).getUpperRightAdjacentTrapecoid().setUpperLeftAdjacentTrapecoid(t1);
									t1.setUpperRightAdjacentTrapecoid(list.get(i).getUpperRightAdjacentTrapecoid());
								}
								
								if(list.get(i).getLowerRightAdjacentTrapecoid() != null) {
									list.get(i).getLowerRightAdjacentTrapecoid().setLowerLeftAdjacentTrapecoid(t2);
									t2.setLowerRightAdjacentTrapecoid(list.get(i).getLowerRightAdjacentTrapecoid());
								}
								
								trapecoidList.add(t1);
								searchGraph.addNode(t1);
								
							} else if(prevUpper.getRightp().getX() == s.getEnd().getX()) {
								
								t1 = prevUpper;
								t2 = new Trapecoid(s, list.get(i).getBottom(), list.get(i).getLeftp(), list.get(i).getRightp());
								
								prevLower.setUpperRightAdjacentTrapecoid(t2);
								t2.setUpperLeftAdjacentTrapecoid(prevLower);
								
								if(list.get(i).getLowerLeftAdjacentTrapecoid() != null) {
									list.get(i).getLowerLeftAdjacentTrapecoid().setLowerRightAdjacentTrapecoid(t2);
									t2.setLowerLeftAdjacentTrapecoid(list.get(i).getLowerLeftAdjacentTrapecoid());
								}
								
								if(list.get(i).getUpperRightAdjacentTrapecoid() != null) {
									list.get(i).getUpperRightAdjacentTrapecoid().setUpperLeftAdjacentTrapecoid(t1);
									t1.setUpperRightAdjacentTrapecoid(list.get(i).getUpperRightAdjacentTrapecoid());
								}
								
								if(list.get(i).getLowerRightAdjacentTrapecoid() != null) {
									list.get(i).getLowerRightAdjacentTrapecoid().setLowerLeftAdjacentTrapecoid(t2);
									t2.setLowerRightAdjacentTrapecoid(list.get(i).getLowerRightAdjacentTrapecoid());
								}
								
								
								trapecoidList.add(t2);
								searchGraph.addNode(t2);
								
							}
								
							SearchNode node = null;
							
							Segment newS = new Segment(s.getStart(), s.getEnd());
							
							try {
								node = this.update(list.get(i), newS);
							} catch (Exception e) {
								e.printStackTrace();
							}
							
							t1.setParent(node);
							t2.setParent(node);
							node.setLeftChild(t1);
							node.setRightChild(t2);

							searchGraph.addNode(newS);
							trapecoidList.remove(list.get(i));
							
//							updateNeighbourships();
									
						} else {
						
							//In diesem Fall werden 3 Trapeze erzeugt
							Trapecoid prevUpper = list.get(i).getNewUpperLeftAdjacentTrapecoid();
							Trapecoid prevLower = list.get(i).getNewLowerLeftAdjacentTrapecoid();
							
							Trapecoid t1 = null;
							Trapecoid t2 = null;
							Trapecoid t3 = null;
							
							if(prevUpper.getTop() == list.get(i).getTop()) {
								
								prevUpper.setRightp(s.getEnd());
								
							} else if (prevLower.getBottom() == list.get(i).getBottom()) {
								
								prevLower.setRightp(s.getEnd());
								
							}
							
							if(prevLower.getRightp().getX() == s.getEnd().getX()) {
								
								t1 = new Trapecoid(list.get(i).getTop(), s, list.get(i).getLeftp(), s.getEnd());
								t2 = prevLower;
								t3 = new Trapecoid(list.get(i).getTop(), list.get(i).getBottom(), s.getEnd(), list.get(i).getRightp());
								
								prevUpper.setLowerRightAdjacentTrapecoid(t1);
								t1.setLowerLeftAdjacentTrapecoid(prevUpper);
								
								if(list.get(i).getUpperLeftAdjacentTrapecoid() != null) {
									list.get(i).getUpperLeftAdjacentTrapecoid().setUpperRightAdjacentTrapecoid(t1);
									t1.setUpperLeftAdjacentTrapecoid(list.get(i).getUpperLeftAdjacentTrapecoid());
								}
								
								t1.setUpperRightAdjacentTrapecoid(t3);
								t3.setUpperLeftAdjacentTrapecoid(t1);
								t2.setLowerRightAdjacentTrapecoid(t3);
								t3.setLowerLeftAdjacentTrapecoid(t2);
								
								if(list.get(i).getUpperRightAdjacentTrapecoid() != null) {
									list.get(i).getUpperRightAdjacentTrapecoid().setUpperLeftAdjacentTrapecoid(t3);
									t3.setUpperRightAdjacentTrapecoid(list.get(i).getUpperRightAdjacentTrapecoid());
								}
								
								if(list.get(i).getLowerRightAdjacentTrapecoid() != null) {
									list.get(i).getLowerRightAdjacentTrapecoid().setLowerLeftAdjacentTrapecoid(t3);
									t3.setLowerRightAdjacentTrapecoid(list.get(i).getLowerRightAdjacentTrapecoid());
								}
								
								
								trapecoidList.add(t1);
								trapecoidList.add(t3);
								searchGraph.addNode(t1);
								searchGraph.addNode(t3);
								
							} else if(prevUpper.getRightp().getX() == s.getEnd().getX()) {
								
								t1 = prevUpper;
								t2 = new Trapecoid(s, list.get(i).getBottom(), list.get(i).getLeftp(), s.getEnd());
								t3 = new Trapecoid(list.get(i).getTop(), list.get(i).getBottom(), s.getEnd(), list.get(i).getRightp());
								
								prevLower.setUpperRightAdjacentTrapecoid(t2);
								t2.setUpperLeftAdjacentTrapecoid(prevLower);
								
								if(list.get(i).getLowerLeftAdjacentTrapecoid() != null) {
									list.get(i).getLowerLeftAdjacentTrapecoid().setLowerRightAdjacentTrapecoid(t2);
									t2.setLowerLeftAdjacentTrapecoid(list.get(i).getLowerLeftAdjacentTrapecoid());
								}
								
								t1.setUpperRightAdjacentTrapecoid(t3);
								t3.setUpperLeftAdjacentTrapecoid(t1);
								t2.setLowerRightAdjacentTrapecoid(t3);
								t3.setLowerLeftAdjacentTrapecoid(t2);
								
								if(list.get(i).getUpperRightAdjacentTrapecoid() != null) {
									list.get(i).getUpperRightAdjacentTrapecoid().setUpperLeftAdjacentTrapecoid(t3);
									t3.setUpperRightAdjacentTrapecoid(list.get(i).getUpperRightAdjacentTrapecoid());
								}
								
								if(list.get(i).getLowerRightAdjacentTrapecoid() != null) {
									list.get(i).getLowerRightAdjacentTrapecoid().setLowerLeftAdjacentTrapecoid(t3);
									t3.setLowerRightAdjacentTrapecoid(list.get(i).getLowerRightAdjacentTrapecoid());
								}
								
								
								trapecoidList.add(t2);
								trapecoidList.add(t3);
								searchGraph.addNode(t2);
								searchGraph.addNode(t3);
								
							}							
							
							SearchNode node = null;
							
							try {
								node = this.update(list.get(i), s.getEnd());
								((Point)node).setUpperExtension(s.getEnd().getUpperExtension());
								((Point)node).setLowerExtension(s.getEnd().getLowerExtension());
							} catch (Exception e) {
								e.printStackTrace();
							}
							
							Segment newS = new Segment(s.getStart(), s.getEnd());
							
							node.setLeftChild(newS);
							node.setRightChild(t3);
							t3.setParent(node);
							newS.setParent(node);

							newS.setLeftChild(t1);
							newS.setRightChild(t2);
							t1.setParent(newS);
							t2.setParent(newS);

							searchGraph.addNode(newS);
							trapecoidList.remove(list.get(i));
							
//							updateNeighbourships();

						}
						
					} else { //das ist der g�ngige Fall, also die Trapeze zwischen i=0 und dem letzten Element der geschnittenen Trapeze
						
						Trapecoid prevUpper = list.get(i).getNewUpperLeftAdjacentTrapecoid();
						Trapecoid prevLower = list.get(i).getNewLowerLeftAdjacentTrapecoid();
						Trapecoid t1 = null;
						Trapecoid t2 = null;
												
						if (prevUpper.getTop() == list.get(i).getTop()) {// && prevUpper.getRightp().getX() != list.get(i).getRightp().getX()) {
							
							t1 = prevUpper;
							t1.setRightp(list.get(i).getRightp());
							
							if(list.get(i).getRightp().getX() < s.getEnd().getX()) {
								t2 = new Trapecoid(s, list.get(i).getBottom(), list.get(i).getLeftp(), s.getEnd());
							} else {
								t2 = new Trapecoid(s, list.get(i).getBottom(), list.get(i).getLeftp(), list.get(i).getRightp());
							}
							
							list.get(i+1).setNewUpperLeftAdjacentTrapecoid(t1);
							list.get(i+1).setNewLowerLeftAdjacentTrapecoid(t2);
							
							if(prevLower.getTop() == t2.getTop() && prevLower.getRightp().getX() == t2.getLeftp().getX()) {
								
								prevLower.setUpperRightAdjacentTrapecoid(t2);
								t2.setUpperLeftAdjacentTrapecoid(prevLower);
								
							}
							
							if(list.get(i).getLowerRightAdjacentTrapecoid() != null) {
								list.get(i).getLowerRightAdjacentTrapecoid().setLowerLeftAdjacentTrapecoid(t2);
								t2.setLowerRightAdjacentTrapecoid(list.get(i).getLowerRightAdjacentTrapecoid());
							}
							
							if(list.get(i).getLowerLeftAdjacentTrapecoid() != null) {
								list.get(i).getLowerLeftAdjacentTrapecoid().setLowerRightAdjacentTrapecoid(t2);
								t2.setLowerLeftAdjacentTrapecoid(list.get(i).getLowerLeftAdjacentTrapecoid());
							}
							
//							t2.setUpperLeftAdjacentTrapecoid(prevLower);
							
							trapecoidList.add(t2);
							searchGraph.addNode(t2);
							
						} 

						else if(prevLower.getBottom() == list.get(i).getBottom()) {// && prevLower.getRightp().getX() != list.get(i).getRightp().getX()) {
							
							t2 = prevLower;
							t2.setRightp(list.get(i).getRightp());
							
							if(list.get(i).getRightp().getX() < s.getEnd().getX()) {
								t1 = new Trapecoid(list.get(i).getTop(), s, list.get(i).getLeftp(), list.get(i).getRightp()); //TODO etzte �nderung
							} else {
								t1 = new Trapecoid(list.get(i).getTop(), s, list.get(i).getLeftp(), s.getEnd()); //TODO etzte �nderung
							}
							
							
							
							list.get(i+1).setNewUpperLeftAdjacentTrapecoid(t1);
							list.get(i+1).setNewLowerLeftAdjacentTrapecoid(t2);
							
							if(prevUpper.getBottom() == t1.getBottom() && prevUpper.getRightp().getX() == t1.getLeftp().getX()) {
								
								prevUpper.setLowerRightAdjacentTrapecoid(t1);
								t1.setLowerLeftAdjacentTrapecoid(prevUpper);
								
							}
							
							if(list.get(i).getUpperRightAdjacentTrapecoid() != null) {
								
								list.get(i).getUpperRightAdjacentTrapecoid().setUpperLeftAdjacentTrapecoid(t1);
								t1.setUpperRightAdjacentTrapecoid(list.get(i).getUpperRightAdjacentTrapecoid());
								
							}
							
							if(list.get(i).getUpperLeftAdjacentTrapecoid() != null) {
								list.get(i).getUpperLeftAdjacentTrapecoid().setUpperRightAdjacentTrapecoid(t1);
								t1.setUpperLeftAdjacentTrapecoid(list.get(i).getUpperLeftAdjacentTrapecoid());
							}
							
//							t1.setLowerLeftAdjacentTrapecoid(prevUpper);
							
							trapecoidList.add(t1);
							searchGraph.addNode(t1);
							
						} else {
							System.out.println("DA GEHT IRGENDWAS SCHIEF - DEBUG ERFORDERLICH");
							System.out.println(s.toString());
						}
						
						
						SearchNode node = null;
						
						Segment newS = new Segment(s.getStart(), s.getEnd());
						
						try {
							node = this.update(list.get(i), newS);
						} catch(Exception e) {
							e.printStackTrace();
						}
						
						t1.setParent(node);
						t2.setParent(node);
						node.setLeftChild(t1);
						node.setRightChild(t2);

						searchGraph.addNode(newS);
						
						trapecoidList.remove(list.get(i));
						
//						updateNeighbourships();
					}
					
				}
				
			}
			
			
			
			int count = 1;
			for(Trapecoid t: trapecoidList) {
				t.setID(count++);
			}
			
//			updateNeighbourships();
			
//			System.out.println(this.toString());
			
//			System.out.println(trapecoidList.size());
			
			//TODO: Hier m�sste man die Sachen zu einer Datenstruktur hinzuf�gen bzw.vorher auch entsprechend kopieren
			
			ArrayList<Trapecoid> listToCopy = new ArrayList<Trapecoid>();
			
			for(Trapecoid t : trapecoidList) {
				listToCopy.add(t);
			}
			
			DirectedAcyclicGraph graphToCopy = new DirectedAcyclicGraph(searchGraph.getSearchNodes());
			
			graphToCopy.setRoot(searchGraph.getRoot());
			
//			ArrayList<SearchNode> nodes = searchGraph.getSearchNodes();
			
//			for(SearchNode n : nodes) {
//				if(n instanceof Point) {
//					System.out
//							.println(((Point) n).getUpperExtension().toString());
//				}
//			}
			HashMap<ArrayList<Trapecoid>, DirectedAcyclicGraph> elem = new HashMap<ArrayList<Trapecoid>,DirectedAcyclicGraph>();
			elem.put(listToCopy, graphToCopy);
			map.put(elemCounter, elem);
			elemCounter++;
			setChanged();
			notifyObservers(map);
		}
		
	}
	
	@Override
	/**
	 * Liefere Inhalt der Trapezkarte und des Suchgraphen
	 */
	public String toString() {
		
		String s = "Anzahl: " + trapecoidList.size() + "\n\n";
		
		for(Trapecoid t : trapecoidList) {
			
			s += t.toString() + "\n\n";
			
		}
		
		s += searchGraph.traverseGraph();
		
		return s;
	}
	
	/**
	 * Aktualisiere den Suchgraphen 
	 * @param ref
	 * @param node
	 * @return
	 * @throws Exception
	 */
	public SearchNode update(SearchNode ref, SearchNode node) throws Exception {
		
		ref = searchGraph.update(ref, node);
		
		if(ref == null) {
			
			throw new Exception("Da ist was schief gegangen");
			
		}
		
		return ref;
		
	}
	
	/**
	 * Hole geschnittene Trapeze anhand des Segmentes s
	 * @param s
	 * @return
	 */
	public ArrayList<Trapecoid> intersectedTrapecoids(Segment s) {
		
		Point p = s.getStart();
		Point q = s.getEnd();
		
//		ArrayList<Trapecoid> trapecoids = new ArrayList<Trapecoid>(trapecoidList);
		ArrayList<Trapecoid> intersectedTrapecoids = new ArrayList<Trapecoid>();
		
		Trapecoid z = null;
		
		try {
			z = (Trapecoid)findTrapecoid(true, p, s);
			intersectedTrapecoids.add(z);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		int j = 0;
		
		Trapecoid t = new Trapecoid (z.getTop(), z.getBottom(), z.getLeftp(), z.getRightp());
		
		while(q.getX() > t.getRightp().getX()) {
				
			Point temp = Helper.isIntersection(new Segment(t.getRightp(), new Point(t.getRightp().getX(),0)), s);
				
			if(t.getRightp().getY() < temp.getY()) {
				
				Trapecoid v = intersectedTrapecoids.get(j).getLowerRightAdjacentTrapecoid();
				
//				System.out
//						.println(v);
				
//				if(v==null) {
//					System.out.println("Query: " + q.getX());
//					
//					if(j>0) {
//						System.out
//								.println("Vorherige Trapeze:");
//						for(Trapecoid w: intersectedTrapecoids) {
//							w.toString();
//						}
//					}	
//					else
//						System.out.println("vorheriges Trapez: " + t.toString());
//					
//					System.out
//							.println("Segment: " + s.toString());
//				}
				intersectedTrapecoids.add(v);
					
			} else {
				
				Trapecoid v = intersectedTrapecoids.get(j).getUpperRightAdjacentTrapecoid();
//				System.out
//				.println(v);
				intersectedTrapecoids.add(v);
			}
			
			j++;
			
			t = intersectedTrapecoids.get(j);
		}
		
		return intersectedTrapecoids;
	}
	
	/**
	 * Suche nach Trapez in Suchdatenstruktur
	 * TODO: Implementierung m�sste dahingehend erweitert werden, dass eine Suchmethode bei der Konstruktion und eine bei der tats�chlichen Suche
	 * genutzt wird -> siehe Kapitel 3.4 Verallgemeinerung
	 * @param p
	 * @return
	 * @throws Exception 
	 */
	public SearchNode findTrapecoid(boolean construction, Point p, Segment s) throws Exception {
		
		if(!construction) {
			s = null;
		}
		
		SearchNode node = searchGraph.getRoot();
		
		while(node.hasChildren()) { 
			
			if(node instanceof Point) {
				
				if(p.getX() < ((Point)node).getX()) {
					
					node = node.getLeftChild();
				
				} else if(p.getX() == ((Point)node).getX()) {
					
					node = node.getRightChild();
					
				} else {
					
					node = node.getRightChild();
					
				}
				
			} else if (node instanceof Segment) {
				
				Point temp = Helper.isIntersection(new Segment(p, new Point(p.getX(),0)), ((Segment)node));
				
				if(p.getY() < temp.getY()) {
					
					node = node.getLeftChild();
//					System.out
//							.println("Linkes Kind normal");
//					System.out
//					.println(node.toString());
					
				} else if(p.getY() > temp.getY()) {
					
					
//					System.out
//					.println("Rechtes Kind normal");
					node = node.getRightChild();
//					System.out
//					.println(node.toString());
						
				} else if(p.getX() == temp.getX() && p.getY() == temp.getY()) {
						
					//Check Slope
					if(!construction) {
						
						return (Segment)node;
						
					} else {
						
						if(s.getGradient() > ((Segment)node).getGradient()) {
							
//							System.out
//							.println("Rechtes Kind spezial");
							node = node.getRightChild();
//							node = node.getLeftChild();
//							System.out
//									.println(node.toString());
							
						} else {
							
//							System.out
//							.println("Linkes Kind spezial");
							node = node.getLeftChild();
//							node = node.getRightChild();
//							System.out
//									.println(node.toString());
							
						}
						
					}
						
				} else {
					throw new Exception("SomethingBadHappened");
				}
					
			}	
		}
			
		if(node instanceof Trapecoid) {
			return (Trapecoid)node;
		} else {
			return null;
		}
	}
	
	
	/**
	 * Liefert eine Permuation der vorhandenen Strecken zur�ck
	 * TODO: In Computational Geometry wird der sogenannte KnuthShuffle vorgeschlagen - mittelfristig sollte der statt der vorhandenen, naiven
	 * Implementierung genutzt werden
	 * @param segments
	 * @return
	 */
	public ArrayList<Segment> getPermutation(ArrayList<Segment> segments) {
		
		if(segments.size() <= 1) {
			return segments;
		}
		
		Random rand = new Random();
		int counter = rand.nextInt(segments.size()-1);
		
		for(int i = 0; i < counter; i++) {
			
			rand = new Random();
			int j = rand.nextInt(segments.size()-1);
			
			Segment s = segments.get(i);
			Segment t = segments.get(j);
			
			segments.set(i, t);
			segments.set(j, s);
			
		}
		
		return segments;
	}

	public DirectedAcyclicGraph getSearchGraph() {
		return searchGraph;
	}

	public void setSearchGraph(DirectedAcyclicGraph searchGraph) {
		this.searchGraph = searchGraph;
	}
	
	public ArrayList<Trapecoid> getTrapecoidList() {
		return this.trapecoidList;
	}
}
	