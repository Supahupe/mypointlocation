package de.hupertz.mypointlocation.model.trapecoidalmap;

import java.io.Serializable;
import java.util.ArrayList;

import de.hupertz.mypointlocation.model.SearchNode;

/**
 * Suchdatenstruktur f�r die Trapezkarte
 * @author Julian
 *
 */
public class DirectedAcyclicGraph implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2101904024583683486L;

	private ArrayList<SearchNode> searchNodes;
	public ArrayList<SearchNode> getSearchNodes() {
		return searchNodes;
	}

	public void setSearchNodes(
			ArrayList<SearchNode> searchNodes) {
		this.searchNodes = searchNodes;
	}

	private SearchNode root;
	
	private SearchNode lastAdded;
	
	public DirectedAcyclicGraph() {
		
		searchNodes = new ArrayList<SearchNode>();
		
	}
	
	public DirectedAcyclicGraph(ArrayList<SearchNode> nodes) {
		
		searchNodes = new ArrayList<SearchNode>();
		
		for(SearchNode n : nodes) {
			searchNodes.add(n);
		}
	}
	
	public SearchNode update(SearchNode ref, SearchNode node) {
		
		if(ref.isRoot()) {
			
			node.setLeftChild(ref.getLeftChild());
			
			if(ref.getLeftChild() != null) {
				node.getLeftChild().setParent(node);
			}
			
			node.setRightChild(ref.getRightChild());
			
			if(ref.getRightChild() != null) {
				node.getRightChild().setParent(node);
			}
			
			node.setRoot(true);
			
			searchNodes.remove(ref);
			root = node;
			searchNodes.add(node);
			
		} else {
			
			for(SearchNode n : ref.getParents()) {
				node.setParent(n);
				
				if(n.getLeftChild() == ref) {
					n.setLeftChild(node);
				} else if (n.getRightChild() == ref){
					n.setRightChild(node);
				}
			}

			
			node.setLeftChild(ref.getLeftChild());
			if(node.getLeftChild()!=null) {
				node.getLeftChild().setParent(node);
			}
			node.setRightChild(ref.getRightChild());
			if(node.getRightChild()!=null) {
				node.getRightChild().setParent(node);
			}
			
			searchNodes.remove(ref);
			searchNodes.add(node);
		}
		
		return node;
	}
	
	@Override public String toString() {
		String s = "Anzahl Knoten: " + searchNodes.size() + "\n";
		
//		s += "Root: " + root.toString() + "\n";
//		s += "Linkes Kind: " + root.getLeftChild().toString() + "\n";
//		s += "Rechtes Kind: " + root.getRightChild().toString() + "\n";
		
		return s;
	}
	
	public String traverseGraph() {
		String s = "\n###########################\n";
		s+=traverseGraph(root);
		s+="\n###########################\n";
		return s;
	}
	
	public String traverseGraph(SearchNode node) {
		String s = "";
		
		if(node == root) {
			s += "Root: " + node.toString() + "\n";
//			s += traverseGraph(node.getLeftChild());
//			s += traverseGraph(node.getRightChild());
		} else {
			s += "Knoten: " + node.toString() + "\n";
		}
		
		s += (node.getLeftChild() == null) ? "Linkes Kind: null\n" : "Linkes Kind: " + node.getLeftChild().toString() +"\n";
		s += (node.getRightChild() == null) ? "Rechtes Kind: null\n" : "Rechtes Kind: " + node.getRightChild().toString() +"\n";
		
		if(node.getLeftChild() != null) {
			
			s += traverseGraph(node.getLeftChild());
			
		}
		
		if(node.getRightChild() != null) {
			
			s+= traverseGraph(node.getRightChild());
		}
		
		return s;
		
	}
	
	public void addNode(SearchNode node) {
		
		if(searchNodes.size()==0) {
			node.setRoot(true);
			setRoot(node);
		}
		
		//TODO: Pr�fbedingungen f�r Node abarbeiten, ggf. umorganisieren
		searchNodes.add(node);
		this.setLastAdded(node);
		
	}
	
	public void clear() {
		
		searchNodes.clear();
		
	}
	
	public int getSize() {
		
		
		return searchNodes.size();
		
	}

	public SearchNode getRoot() {
		return root;
	}

	public void setRoot(SearchNode root) {
		root.setRoot(true);
		this.root = root;
		
	}

	public SearchNode getLastAdded() {
		return lastAdded;
	}

	public void setLastAdded(SearchNode lastAdded) {
		this.lastAdded = lastAdded;
	}
}
