package de.hupertz.mypointlocation.model.trapecoidalmap;

import de.hupertz.mypointlocation.model.Point;
import de.hupertz.mypointlocation.model.SearchNode;
import de.hupertz.mypointlocation.model.Segment;

/**
 * Datenstruktur eines Trapezes
 * @author Julian
 *
 */
public class Trapecoid extends SearchNode {
	
	/**
	 * 
	 */
	private Segment top;
	private Segment bottom;
	private Point leftp;
	private Point rightp;
	
	private Trapecoid upperRightAdjacentTrapecoid = null;
	private Trapecoid upperLeftAdjacentTrapecoid = null;
	private Trapecoid lowerRightAdjacentTrapecoid = null;
	private Trapecoid lowerLeftAdjacentTrapecoid = null;
	
	private Trapecoid newUpperLeftAdjacentTrapecoid = null;
	private Trapecoid newLowerLeftAdjacentTrapecoid = null;
	
	public boolean leftDeg = false;
	public boolean rightDeg = false;
	
	private boolean isObstacle = false;

	public boolean isObstacle() {
		return isObstacle;
	}

	public void setObstacle(boolean isObstacle) {
		this.isObstacle = isObstacle;
	}

	private int id = -1;
	
	private String strId = "";
	
	public String getStrId() {
		return strId;
	}

	public void setStrId(String strId) {
		this.strId = strId;
	}

	public Trapecoid getUpperRightAdjacentTrapecoid() {
		return upperRightAdjacentTrapecoid;
	}

	public void setUpperRightAdjacentTrapecoid(
			Trapecoid upperRightAjacentTrapecoid) {
		this.upperRightAdjacentTrapecoid = upperRightAjacentTrapecoid;
	}
	
	public Trapecoid getUpperLeftAdjacentTrapecoid() {
		return upperLeftAdjacentTrapecoid;
	}

	public void setUpperLeftAdjacentTrapecoid(
			Trapecoid upperLeftAdjacentTrapecoid) {
		this.upperLeftAdjacentTrapecoid = upperLeftAdjacentTrapecoid;
	}
	
	public Trapecoid getLowerRightAdjacentTrapecoid() {
		return lowerRightAdjacentTrapecoid;
	}

	public void setLowerRightAdjacentTrapecoid(
			Trapecoid lowerRightAdjacentTrapecoid) {
		this.lowerRightAdjacentTrapecoid = lowerRightAdjacentTrapecoid;
	}
	
	public Trapecoid getLowerLeftAdjacentTrapecoid() {
		return lowerLeftAdjacentTrapecoid;
	}

	public void setLowerLeftAdjacentTrapecoid(
			Trapecoid lowerLeftAdjacentTrapecoid) {
		this.lowerLeftAdjacentTrapecoid = lowerLeftAdjacentTrapecoid;
	}
	
	public Trapecoid (double mX, double mY, double mHeight, double mWidth) {
		
		top = new Segment(new Point(mX, mY), new Point(mWidth, mY));
		bottom = new Segment(new Point(mX, mHeight), new Point(mWidth, mHeight));
		
		if(top.getStart().getX() <= bottom.getStart().getX()) {
			leftp = top.getStart();
		} else {
			leftp = bottom.getStart();
		}
		
		if(top.getEnd().getX() >= bottom.getEnd().getX()) {
			rightp = bottom.getEnd();
		} else {
			rightp = top.getEnd();
		}
		
		if(top.getStart().equals(bottom.getStart())) {
			leftDeg = true;
		}
		
		if(top.getEnd().equals(bottom.getEnd())) {
			rightDeg = true;
		}
		
	}
	
	public Trapecoid(Segment top, Segment bottom) {
		this.top = top;
		this.bottom = bottom;
		
		if(top.getStart().getX() <= bottom.getStart().getX()) {
			leftp = top.getStart();
		} else {
			leftp = bottom.getStart();
		}
		
		if(top.getEnd().getX() >= bottom.getEnd().getX()) {
			rightp = bottom.getEnd();
		} else {
			rightp = top.getEnd();
		}
		
		if(top.getStart().equals(bottom.getStart())) {
			leftDeg = true;
		}
		
		if(top.getEnd().equals(bottom.getEnd())) {
			rightDeg = true;
		}
		
	}
	
	public Trapecoid(Segment top, Segment bottom, Point leftp, Point rightp) {
		this.top = top;
		this.bottom = bottom;
		this.leftp = leftp;
		this.rightp = rightp;
		
		if(top.getStart().equals(bottom.getStart())) {
			leftDeg = true;
		}
		
		if(top.getEnd().equals(bottom.getEnd())) {
			rightDeg = true;
		}
		
	}

	public Segment getTop() {
		return top;
	}

	public void setTop(Segment top) {
		this.top = top;
	}

	public Segment getBottom() {
		return bottom;
	}

	public void setBottom(Segment bottom) {
		this.bottom = bottom;
	}

	public Point getLeftp() {
		return leftp;
	}

	public void setLeftp(Point leftp) {
		this.leftp = leftp;
	}

	public Point getRightp() {
		return rightp;
	}

	public void setRightp(Point rightp) {
		this.rightp = rightp;
	}

	public int getID() {
		return id;
	}

	public void setID(int id) {
		this.id = id;
		this.setStrId(""+this.id);
	}

	@Override
	public String toString() {
		String s = "";
		
		s += "Trapez: " + "ID " + this.id + "\n";
		s += (this.strId.equals("")) ? "Alternativer Bezeichner:\n" : "Alternativer Bezeichner: " + strId +"\n";
		s += (!isObstacle) ? "Hindernis: nein\n" : "Hindernis: ja\n";
		s += "Top-"+top.toString()+"\n";
		s += "Bottom-"+bottom.toString()+"\n";
		s += "LeftPoint: "+leftp.toString()+"\n";
		s += "RightPoint: "+rightp.toString()+"\n";
		s += "Linker oberer Nachbar: ";
		s += (this.getUpperLeftAdjacentTrapecoid()!=null) ? this.getUpperLeftAdjacentTrapecoid().getID()+"\n" : "null\n";
		s += "Linker unterer Nachbar: ";
		s += (this.getLowerLeftAdjacentTrapecoid()!=null) ? this.getLowerLeftAdjacentTrapecoid().getID()+"\n" : "null\n";
		s += "Rechter oberer Nachbar: ";
		s += (this.getUpperRightAdjacentTrapecoid()!=null) ? this.getUpperRightAdjacentTrapecoid().getID()+"\n" : "null\n";
		s += "Rechter unterer Nachbar: ";
		s += (this.getLowerRightAdjacentTrapecoid()!=null) ? this.getLowerRightAdjacentTrapecoid().getID()+"\n" : "null\n";
		
		return s;
	}
	
	public String getTextAreaDescription() {
		
		String s = "";
		
		s += "Trapez: " + "ID " + this.id + "\n";
		s += (this.strId.equals("")) ? "Alternativer Bezeichner:\n" : "Alternativer Bezeichner: " + strId +"\n";
		s += (!isObstacle) ? "Hindernis: nein\n" : "Hindernis: ja\n";
		s += "Top-"+top.toString()+"\n";
		s += "Bottom-"+bottom.toString()+"\n";
		s += "LeftPoint: "+leftp.toString()+"\n";
		s += "RightPoint: "+rightp.toString()+"\n";
		s += "Linker oberer Nachbar: ";
		s += (this.getUpperLeftAdjacentTrapecoid()!=null) ? this.getUpperLeftAdjacentTrapecoid().getID()+"\n" : "null\n";
		s += "Linker unterer Nachbar: ";
		s += (this.getLowerLeftAdjacentTrapecoid()!=null) ? this.getLowerLeftAdjacentTrapecoid().getID()+"\n" : "null\n";
		s += "Rechter oberer Nachbar: ";
		s += (this.getUpperRightAdjacentTrapecoid()!=null) ? this.getUpperRightAdjacentTrapecoid().getID()+"\n" : "null\n";
		s += "Rechter unterer Nachbar: ";
		s += (this.getLowerRightAdjacentTrapecoid()!=null) ? this.getLowerRightAdjacentTrapecoid().getID()+"\n" : "null\n";
		
		return s;
	}

	public Trapecoid getNewUpperLeftAdjacentTrapecoid() {
		return newUpperLeftAdjacentTrapecoid;
	}

	public void setNewUpperLeftAdjacentTrapecoid(
			Trapecoid newUpperLeftAdjacentTrapecoid) {
		this.newUpperLeftAdjacentTrapecoid = newUpperLeftAdjacentTrapecoid;
	}

	public Trapecoid getNewLowerLeftAdjacentTrapecoid() {
		return newLowerLeftAdjacentTrapecoid;
	}

	public void setNewLowerLeftAdjacentTrapecoid(
			Trapecoid newLowerLeftAdjacentTrapecoid) {
		this.newLowerLeftAdjacentTrapecoid = newLowerLeftAdjacentTrapecoid;
	}
	
}
