package de.hupertz.mypointlocation.model.simpleapproach;

import java.util.ArrayList;
import java.util.Collections;

import de.hupertz.mypointlocation.model.Face;
import de.hupertz.mypointlocation.model.Point;
import de.hupertz.mypointlocation.model.Segment;
import de.hupertz.mypointlocation.model.Slab;
import de.hupertz.mypointlocation.utils.Helper;

/**
 * Klasse f�r naiven Algorithmus
 * @author Julian
 *
 */
public class SimpleApproachDataStructure {
	
	double mWidth;
	double mHeight;
	
	private ArrayList<Slab> mSlabsList;
	private ArrayList<Double> xArray;
	private ArrayList<Double> yArray;
	private ArrayList<Segment> mSegmentsList;
	
	public SimpleApproachDataStructure(double width, double heigth) {
		
		this.mWidth = width;
		this.mHeight = heigth;
		
	}
	
	/**
	 * Erzeugt naive Datenstruktur
	 * @param pointList
	 * @param segmentList
	 * @param slabSegmentList
	 * @return
	 */
	public ArrayList<Slab> createDataStructure(ArrayList<Point> pointList, ArrayList<Segment> segmentList, ArrayList<Slab> slabSegmentList) {
		
		mSegmentsList = segmentList;
		mSlabsList = slabSegmentList;
		
		Collections.sort(mSegmentsList);
		
		ArrayList<Segment> intersectionSegments = new ArrayList<Segment>();
		ArrayList<Face> facesList = new ArrayList<Face>();
		
		int count = 1;
		
		boolean flag = false;
		
		for(Slab s : mSlabsList) {
			
			intersectionSegments.clear();
			facesList.clear();
			flag = false;
			
			for(Segment seg : mSegmentsList) {
				
				if(s.getMiddleSegment().getStart().getX() < seg.getStart().getX()
						||
				   s.getMiddleSegment().getStart().getX() > seg.getEnd().getX()) {
					
				} else {
					flag = true;
					Point pLeft = Helper.isIntersection(s.getLeftSegment().getStart(), s.getLeftSegment().getEnd(), 
							seg.getStart(), seg.getEnd());
					Point pMid = Helper.isIntersection(s.getMiddleSegment().getStart(), s.getMiddleSegment().getEnd(), 
							seg.getStart(), seg.getEnd());
					Point pRight = Helper.isIntersection(s.getRightSegment().getStart(), s.getRightSegment().getEnd(), 
							seg.getStart(), seg.getEnd());
					
					intersectionSegments.add(new Segment(pLeft, pMid, pRight));
				}
			}
			
			if( flag == false) {
				Face f = new Face(count++, 
						new Segment(new Point(s.getLeftSegment().getEnd().getX(), 0), new Point (s.getRightSegment().getEnd().getX(), 0)), 
						new Segment(new Point(s.getLeftSegment().getEnd().getX(), mHeight), new Point(s.getRightSegment().getEnd().getX(), mHeight)));
				s.addFace(f);
				
			} else {
				
				int border = intersectionSegments.size();
				
				for(int i=0; i< border; i++) {
					if(i==0) {
						Face f = new Face(count++, 
								new Segment (
										new Point(s.getLeftSegment().getStart().getX(),0), 
										new Point(s.getRightSegment().getEnd().getX(),0)),
								intersectionSegments.get(i));
						s.addFace(f);
						
					} else if(i==(border-1)) {
						Face f = new Face(count++, 
								intersectionSegments.get(i-1),
								intersectionSegments.get(i));
						s.addFace(f);
						
						f = new Face(count++, 
								intersectionSegments.get(i),
								new Segment (
										new Point(s.getLeftSegment().getStart().getX(),mHeight), 
										new Point(s.getRightSegment().getEnd().getX(),mHeight)));
						s.addFace(f);
						
					} else {
						Face f = new Face(count++, 
								intersectionSegments.get(i-1),
								intersectionSegments.get(i));
						s.addFace(f);
						
					}
					
					if(border==1) {
						Face f = new Face(count++, 
								intersectionSegments.get(i),
								new Segment (
										new Point(s.getLeftSegment().getStart().getX(),mHeight), 
										new Point(s.getRightSegment().getEnd().getX(),mHeight)));
						s.addFace(f);
					}
					
				}
			}
		}
		
		return mSlabsList;
	}
	
	/**
	 * Bin�re Suche in den einzelnen Scheiben
	 * @param slabList
	 * @param lowerbound
	 * @param upperbound
	 * @param key
	 * @return
	 */
	public Face binarySearch(ArrayList<Slab> slabList, int lowerbound, int upperbound, Point key) {
		
		int position = (lowerbound + upperbound) / 2;
		
		boolean flag = false;
		
		while(!flag) {
			
			if((slabList.get(position).getRightSegment().getStart().getX() < key.getX())
					&&
			   (slabList.get(position).getLeftSegment().getStart().getX() < key.getX())) {
				
				lowerbound = position + 1;
				
			} else if((slabList.get(position).getLeftSegment().getStart().getX() > key.getX())
					&&
					(slabList.get(position).getRightSegment().getStart().getX() > key.getX())) {
				
				upperbound = position - 1;
				
			} else {
				flag = true;
			}
			
			if(!flag) {
				position = (lowerbound + upperbound) / 2;
			}
		}
		
		return slabList.get(position).binarySearch(key);
	}

	public ArrayList<Slab> getmSlabsList() {
		
		return mSlabsList;
		
	}

	public void setmSlabsList(ArrayList<Slab> mSlabsList) {
		
		this.mSlabsList = mSlabsList;
		
	}
	

	public ArrayList<Double> getxArray() {
		
		return xArray;
		
	}

	public void setxArray(ArrayList<Double> xArray) {
		
		this.xArray = xArray;
		
	}

	public ArrayList<Double> getyArray() {
		
		return yArray;
		
	}

	public void setyArray(ArrayList<Double> yArray) {
		
		this.yArray = yArray;
		
	}
}
