package de.hupertz.mypointlocation.model;

import java.util.ArrayList;

/**
 * Such-Knoten, abstrakte Oberklasse
 * @author Julian
 *
 */
public abstract class SearchNode {
	
	protected boolean isRoot;
	protected SearchNode parent;
	protected ArrayList<SearchNode> furtherParents;
	protected SearchNode leftChild;
	protected SearchNode rightChild;
	
	public SearchNode() {
		leftChild = null;
		rightChild = null;
		furtherParents = new ArrayList<SearchNode>();
	}
	
	public boolean hasChildren() {
		
		if(leftChild != null)
			return true;
		else if(rightChild != null)
			return true;
		else
			return false;			
	}
	
	public ArrayList<SearchNode> getFurtherParents() {
		return furtherParents;
	}

	public void setFurtherParents(ArrayList<SearchNode> furtherParents) {
		this.furtherParents = furtherParents;
	}
	
	public boolean isRoot() {
		return isRoot;
	}
	
	public void setRoot(boolean isRoot) {
		this.isRoot = isRoot;
	}
	
	public SearchNode getParent() {
		return parent;
	}
	
	public ArrayList<SearchNode> getParents() {
		ArrayList<SearchNode> parents = new ArrayList<SearchNode>();
		parents.add(this.parent);
		
		for(SearchNode n : furtherParents) {
			parents.add(n);
		}
		
		return parents;
	}
	
	public void setParent(SearchNode parent) {
		if(this.parent == null)
			this.parent = parent;
		else
			furtherParents.add(parent);	
	}
	
	public SearchNode getLeftChild() {
		return leftChild;
	}
	
	public void setLeftChild(SearchNode leftChild) {
		this.leftChild = leftChild;
	}
	
	public SearchNode getRightChild() {
		return rightChild;
	}
	
	public void setRightChild(SearchNode rightChild) {
		this.rightChild = rightChild;
	}
}